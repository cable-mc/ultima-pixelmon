package com.cable.ultimapixelmon.appendage

import com.cable.library.data.getSimpleData
import com.cable.library.text.add
import com.cable.library.text.onClick
import com.cable.ultimapixelmon.config.PixMessages
import com.cable.ultimapixelmon.listener.WaypointListener
import com.cable.ultimaquests.api.EightFacing
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.task.ITaskAppendage
import com.cable.ultimaquests.api.model.task.Task
import com.cable.ultimaquests.api.storage.IProgressStore
import com.cable.ultimaquests.internal.storage.CommonQuestData
import java.util.UUID
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.pow
import kotlin.math.sin
import kotlin.math.sqrt
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.util.ResourceLocation
import net.minecraft.util.math.vector.Vector3d
import net.minecraft.util.text.IFormattableTextComponent

class Waypoint : ITaskAppendage<Task<*, *>> {
    companion object {
        const val SELECTED_TASK = "selectedTask"
        const val SELECTED_STORE = "selectedStore"
    }

    override fun getModelClass() = Task::class.java

    @Transient
    lateinit var task: Task<*, *>

    override fun setModel(t: Task<*, *>) {
        this.task = t
    }

    @EditorField(displayLabel = "Waypoint Dimension", description = "The waypoint dimension (such as minecraft:overworld). If all waypoint values are blank or zero, there will be no waypoint marker.")
    var waypointDimension = ""
    @EditorField(displayLabel = "Waypoint X", description = "The waypoint x coordinate. If all waypoint values are zero, there will be no waypoint marker.")
    var waypointX = 0.0
    @EditorField(displayLabel = "Waypoint Z", description = "The waypoint z coordinate. If all waypoint values are zero, there will be no waypoint marker.")
    var waypointZ = 0.0
    @EditorField(displayLabel = "Waypoint Radius", description = "The waypoint's radius. When the player is within this number of blocks, the icon will change to show they are at the destination. Defaults to 10.")
    var waypointRadius = 10.0
    @EditorField(displayLabel = "Track Automatically", description = "Whether or not once this task becomes active due to the player's actions it will be made the tracked task. Defaults to false.")
    val trackAutomatically = false

    override fun transformTaskHover(viewer: ServerPlayerEntity, target: UUID, store: IProgressStore, taskHover: IFormattableTextComponent): IFormattableTextComponent {
        val text = super.transformTaskHover(viewer, target, store, taskHover)
        return if (viewer.uuid == target) {
            val m = getSimpleData<PixMessages>()
            val common = getSimpleData<CommonQuestData>(target)
            if (common.getProperty(SELECTED_TASK) != task.name) {
                text.add("\n\n${
                    if (isEmpty) {
                        m.setSelectedTaskMessage
                    } else {
                        m.setSelectedTaskWithWaypointMessage
                    }
                }")
            } else {
                text.add("\n\n${m.unsetSelectedTaskMessage}")
            }
        } else {
            text
        }
    }

    override fun transformTaskText(viewer: ServerPlayerEntity, target: UUID, store: IProgressStore, taskText: IFormattableTextComponent): IFormattableTextComponent {
        val text = super.transformTaskText(viewer, target, store, taskText)
        return if (viewer.uuid == target) {
            val common = getSimpleData<CommonQuestData>(target)
            text.onClick {
                common.doAndSave {
                    if (getProperty(SELECTED_TASK) == task.name) {
                        deleteProperty(SELECTED_TASK)
                        deleteProperty(SELECTED_STORE)
                        selectedQuest = null
                    } else {
                        setProperty(SELECTED_TASK, task.name)
                        setProperty(SELECTED_STORE, store.getStoreProvider().getName())
                        selectedQuest = task.getQuest().name
                    }

                }
                WaypointListener.waypoints[target] = this
            }
        } else {
            text
        }
    }

    val isEmpty: Boolean get() = waypointDimension == "" && waypointX == 0.0 && waypointZ == 0.0

    fun getDistanceAndDirection(player: ServerPlayerEntity): Pair<Double, EightFacing>? {
        if (player.getLevel().dimension().location() != ResourceLocation(waypointDimension)) {
            return null
        }

        val difference = Vector3d(waypointX - player.x, 0.0, waypointZ - player.z)
        val trueDifference = Vector3d(difference.z, 0.0, difference.x).normalize()
        val trueLook = Vector3d(player.lookAngle.z, 0.0, player.lookAngle.x).normalize()

        val dot = trueDifference.dot(trueLook)
        val angleRadians = Math.acos(dot)
        val angleDegrees = Math.toDegrees(angleRadians)

        /**
         * Rotates trueLook by the given angle (anti-clockwise) and returns how far away the result was from trueDifference
         * This is to help us figure out if we should be turning left or right
         */
        fun differenceWhenSpun(theta: Double): Double {
            val x = trueLook.x() * cos(theta) - trueLook.z() * sin(theta)
            val y = 0.0
            val z = trueLook.x() * sin(theta) + trueLook.z() * cos(theta)
            val vec = Vector3d(x, y, z)

            val vectorFromTarget = Vector3d(vec.x - trueDifference.x, 0.0, vec.z - trueDifference.z)
            return vectorFromTarget.lengthSqr()
        }

        val distanceWhenSpunLeft = differenceWhenSpun(angleRadians)
        val distanceWhenSpunRight = differenceWhenSpun(2 * Math.PI - angleRadians)

        val isLeft = distanceWhenSpunLeft < distanceWhenSpunRight

        val eighth = 45 / 2.0
        val dir = when {
            angleDegrees < eighth -> EightFacing.NORTH
            angleDegrees < 45 + eighth -> if (isLeft) EightFacing.NORTH_WEST else EightFacing.NORTH_EAST
            angleDegrees < 90 + eighth -> if (isLeft) EightFacing.WEST else EightFacing.EAST
            angleDegrees < 135 + eighth -> if (isLeft) EightFacing.SOUTH_WEST else EightFacing.SOUTH_EAST
            else -> EightFacing.SOUTH
        }
        val dist = sqrt(abs(player.x - waypointX).pow(2) + abs(player.z - waypointZ).pow(2))

        return dist to dir
    }
}