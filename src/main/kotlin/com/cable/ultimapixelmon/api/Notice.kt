package com.cable.ultimapixelmon.api

import com.pixelmonmod.api.pokemon.PokemonSpecification
import com.pixelmonmod.api.pokemon.PokemonSpecificationProxy
import com.pixelmonmod.pixelmon.api.overlay.notice.EnumOverlayLayout
import com.pixelmonmod.pixelmon.api.overlay.notice.NoticeOverlay
import com.pixelmonmod.pixelmon.api.pokemon.species.Species
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.util.text.ITextComponent

fun ServerPlayerEntity.sendNotice(
    lines: Iterable<ITextComponent>,
    spec: PokemonSpecification,
    useModel: Boolean = false,
    layout: EnumOverlayLayout = EnumOverlayLayout.LEFT_AND_RIGHT
) {
    NoticeOverlay.builder()
        .setLayout(layout)
        .setLines(lines.toList())
        .also {
            if (useModel) {
                it.setPokemon3D(spec)
            } else {
                it.setPokemonSprite(spec)
            }
        }
        .sendTo(this)
}

fun ServerPlayerEntity.sendNotice(
    lines: Iterable<ITextComponent>,
    species: Species,
    useModel: Boolean = false,
    layout: EnumOverlayLayout = EnumOverlayLayout.LEFT_AND_RIGHT
) = sendNotice(lines, PokemonSpecificationProxy.create(species.name), useModel, layout)

fun ServerPlayerEntity.sendNotice(
    lines: Iterable<ITextComponent>,
    stack: ItemStack,
    layout: EnumOverlayLayout = EnumOverlayLayout.LEFT_AND_RIGHT
) {
    NoticeOverlay.builder()
        .setLayout(layout)
        .setLines(lines.toList())
        .setItemStack(stack)
        .sendTo(this)
}

fun ServerPlayerEntity.sendNotice(
    lines: Iterable<ITextComponent>,
    item: Item,
    layout: EnumOverlayLayout = EnumOverlayLayout.LEFT_AND_RIGHT
) {
    sendNotice(lines, ItemStack(item), layout)
}

fun ServerPlayerEntity.hideNotice() = NoticeOverlay.hide(this)