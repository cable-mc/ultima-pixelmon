package com.cable.ultimapixelmon.gson

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.pixelmonmod.api.pokemon.PokemonSpecification
import com.pixelmonmod.api.pokemon.PokemonSpecificationProxy
import java.lang.reflect.Type

object PokemonSpecAdapter : JsonSerializer<PokemonSpecification>, JsonDeserializer<PokemonSpecification> {
    override fun serialize(spec: PokemonSpecification, type: Type, ctx: JsonSerializationContext): JsonElement {
        return JsonPrimitive(spec.toString())
    }

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): PokemonSpecification {
        return PokemonSpecificationProxy.create(*json.asString.split(" ").toTypedArray())
    }
}