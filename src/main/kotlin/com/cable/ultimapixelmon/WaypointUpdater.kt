package com.cable.ultimapixelmon

import com.cable.library.data.getData
import com.cable.library.data.getSimpleData
import com.cable.library.text.bold
import com.cable.library.text.red
import com.cable.library.text.text
import com.cable.ultimapixelmon.api.hideNotice
import com.cable.ultimapixelmon.api.sendNotice
import com.cable.ultimapixelmon.appendage.Waypoint
import com.cable.ultimapixelmon.appendage.Waypoint.Companion.SELECTED_STORE
import com.cable.ultimapixelmon.appendage.Waypoint.Companion.SELECTED_TASK
import com.cable.ultimapixelmon.config.PixMessages
import com.cable.ultimapixelmon.listener.WaypointListener
import com.cable.ultimaquests.api.UltimaQuests
import com.cable.ultimaquests.internal.storage.CommonQuestData
import com.cable.ultimaquests.internal.util.getOnlinePlayer
import com.pixelmonmod.pixelmon.api.overlay.notice.EnumOverlayLayout
import net.minecraft.entity.player.ServerPlayerEntity
import java.util.UUID
import net.minecraft.item.Items

object WaypointUpdater {
    fun getActiveWaypoint(uuid: UUID): Waypoint? {
        val player = uuid.getOnlinePlayer()
        val stores = UltimaQuests.api.getProgressStores(uuid)
        val common = getSimpleData<CommonQuestData>(uuid)
        val selectedQuest = common.selectedQuest ?: return null
        val quest = UltimaQuests.api.getQuestRegistry().getQuest(selectedQuest)
        val selectedTask = common.getProperty(SELECTED_TASK) ?: return null
        val selectedStore = common.getProperty(SELECTED_STORE) ?: return null
        val task = quest?.findTask(selectedTask)
        val store = stores.find { it.getStoreProvider().getName() == selectedStore } ?: return null
        val questProgress = quest?.let { store.getProgressFor(it) }

        if (quest == null || (player != null && !quest.canSee(player, questProgress, true))) {
            common.doAndSave { this.selectedQuest = null ; deleteProperty(SELECTED_TASK) }
            player?.let { removeWaypoint(it) }
            return null
        }

        questProgress ?: return null

        if (task == null) {
            common.doAndSave { deleteProperty(SELECTED_TASK) }
            player?.let { removeWaypoint(it) }
            return null
        }

        val taskProgress = questProgress.remainingTasks[task]
        if (taskProgress == null) {
            // Try assigning a new waypoint
            val nextTask = questProgress.remainingTasks.keys.firstOrNull { !it.isHidden }
            return if (nextTask != null) {
                common.doAndSave { this.setProperty(SELECTED_TASK, nextTask.name) }
                nextTask.getAppendage()
            } else {
                common.doAndSave {
                    player?.let { removeWaypoint(it) }
                    this.deleteProperty(SELECTED_TASK)
                    this.deleteProperty(SELECTED_STORE)
                }
                null
            }
        } else {
            return task.getAppendage()
        }
    }

    fun updateWaypoint(player: ServerPlayerEntity, waypoint: Waypoint) {
        val colour = getSimpleData<PixMessages>().waypointTextColour
        val (distance, direction) = waypoint.getDistanceAndDirection(player) ?: (null to null)
        val symbol = if (waypoint.isEmpty) {
            null
        } else if (distance == null || direction == null) {
            "-".red()
        } else {
            if (distance <= waypoint.waypointRadius) {
                text(colour, "✔")
            } else {
                text(colour, direction.arrow)
            }
        }

        val common = player.getData<CommonQuestData>()
        val selectedStore = common.getProperty(SELECTED_STORE) ?: return untrackWaypointing(player)
        val store = UltimaQuests.api.getProgressStoreProviderByName(selectedStore)?.getProgressStore(player.uuid) ?: return untrackWaypointing(player)
        val taskProgress = store.getProgressFor(waypoint.task) ?: return untrackWaypointing(player)
        val taskLines = waypoint.task.title.split("\n").map { text(colour, taskProgress.applyPlaceholders(it)).bold() }
        val lines = if (symbol == null) taskLines else (taskLines + symbol)

        try {
            player.sendNotice(
                item = Items.AIR,
                layout = EnumOverlayLayout.LEFT_AND_RIGHT,
                lines = lines
            )
        } catch (e: Exception) {}
    }

    fun removeWaypoint(player: ServerPlayerEntity) {
        player.hideNotice()
    }

    fun untrackWaypointing(player: ServerPlayerEntity) {
        WaypointListener.waypoints.remove(player.uuid)
        removeWaypoint(player)
    }
}