package com.cable.ultimapixelmon.config

import com.cable.library.data.SimpleData

class PixConfig(
    override val fileMode: FileMode = FileMode.SINGULAR,
    override val root: String = "config/ultimaquests/pixelmon/ultima-pixelmon.json"
) : SimpleData() {
    var secondsBetweenWaypointUpdates = 0.5
}