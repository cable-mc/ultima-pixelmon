package com.cable.ultimapixelmon.config

import com.cable.library.data.SimpleData
import net.minecraft.util.text.TextFormatting

class PixMessages(
    override val fileMode: FileMode = FileMode.SINGULAR,
    override val root: String = "config/ultimaquests/pixelmon/messages.json"
) : SimpleData() {
    var setSelectedTaskMessage = "Click to pin this task."
    val setSelectedTaskWithWaypointMessage = "Click to pin this task and navigate to it."
    val unsetSelectedTaskMessage = "Click to unfollow this task."
    val trainerIsQuestNPCMessage = "&cThat trainer is reserved for a quest."
    var waypointTextColour = TextFormatting.BLUE
}