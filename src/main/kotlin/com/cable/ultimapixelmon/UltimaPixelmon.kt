package com.cable.ultimapixelmon

import com.cable.library.data.getSimpleData
import com.cable.ultimapixelmon.appendage.Waypoint
import com.cable.ultimapixelmon.command.UltimaPixelmonCommand
import com.cable.ultimapixelmon.config.PixConfig
import com.cable.ultimapixelmon.config.PixMessages
import com.cable.ultimapixelmon.gson.PokemonSpecAdapter
import com.cable.ultimapixelmon.listener.*
import com.cable.ultimapixelmon.model.actions.ApplySpec
import com.cable.ultimapixelmon.model.actions.BoxPokemon
import com.cable.ultimapixelmon.model.actions.ChangeMoney
import com.cable.ultimapixelmon.model.actions.GivePokemon
import com.cable.ultimapixelmon.model.actions.HealParty
import com.cable.ultimapixelmon.model.actions.ShowChoices
import com.cable.ultimapixelmon.model.actions.ShowDialogue
import com.cable.ultimapixelmon.model.actions.SpawnAndBattle
import com.cable.ultimapixelmon.model.actions.TakePokemon
import com.cable.ultimapixelmon.model.requirements.CanBattle
import com.cable.ultimapixelmon.model.requirements.HaveMoney
import com.cable.ultimapixelmon.model.requirements.HavePokedexCount
import com.cable.ultimapixelmon.model.requirements.HavePokedexStatus
import com.cable.ultimapixelmon.model.requirements.HavePokemonInLevelRange
import com.cable.ultimapixelmon.model.requirements.HavePokemonInPC
import com.cable.ultimapixelmon.model.requirements.HavePokemonInParty
import com.cable.ultimapixelmon.model.requirements.IsRidingPokemon
import com.cable.ultimapixelmon.model.tasks.*
import com.cable.ultimapixelmon.spec.MarkRequirement
import com.cable.ultimapixelmon.spec.UnmarkRequirement
import com.cable.ultimaquests.UltimaQuestsMod
import com.cable.ultimaquests.api.UltimaQuests
import com.cable.ultimaquests.api.event.ModelGsonEvent
import com.cable.ultimaquests.api.event.QuestModelRegistrationEvent
import com.cable.ultimaquests.api.event.StorageGsonEvent
import com.pixelmonmod.api.pokemon.PokemonSpecification
import com.pixelmonmod.api.pokemon.PokemonSpecificationProxy
import com.pixelmonmod.pixelmon.Pixelmon
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.RegisterCommandsEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext
import org.apache.logging.log4j.LogManager

@Mod(UltimaPixelmon.MODID)
class UltimaPixelmon {
    companion object {
        const val MODID = "ultimapixelmon"
        const val NAME = "UltimaPixelmon"

        val BLANK_SPEC by lazy { PokemonSpecificationProxy.create("") }

        private val LOGGER = LogManager.getLogger(NAME)
    }

    init {
        val ctx = FMLJavaModLoadingContext.get()
        MinecraftForge.EVENT_BUS.register(this)
        ctx.modEventBus.addListener(this::onConstruction)
        UltimaQuestsMod.EVENT_BUS.register(this)
    }

    @SubscribeEvent
    fun onModelRegistration(event: QuestModelRegistrationEvent) {
        LOGGER.info("Initializing Pixelmon model definitions...")

        with(UltimaQuests.api) {
            defineTaskType("catch-pokemon", CatchPokemon::class.java)
            defineTaskType("chat-to-entity", ChatToEntity::class.java)
            defineTaskType("select-choice", SelectChoice::class.java)
            defineTaskType("evolve-pokemon", EvolvePokemon::class.java)
            defineTaskType("hatch-pokemon", HatchPokemon::class.java)
            defineTaskType("fish-pokemon", FishPokemon::class.java)
            defineTaskType("defeat-trainer", DefeatTrainer::class.java)
            defineTaskType("defeat-wild-pokemon", DefeatWildPokemon::class.java)
//            defineTaskType("defeat-den", DefeatDen::class.java)
            defineTaskType("lose-to-trainer", LoseToTrainer::class.java)
            defineTaskType("lose-to-wild-pokemon", LoseToWildPokemon::class.java)
//            defineTaskType("lose-to-den", LoseToDen::class.java)
            defineTaskType("run-out-of-pokemon", RunOutOfPokemon::class.java)
            defineTaskType("level-up-pokemon", LevelUpPokemon::class.java)
            defineTaskType("learn-move", LearnMove::class.java)
            defineTaskType("cook-curry", CookCurry::class.java)
            defineTaskType("receive-item-drop", ReceiveItemDrop::class.java)
            defineTaskType("take-photo", TakePhoto::class.java)

            defineRequirementType("have-pokedex-count", HavePokedexCount::class.java)
            defineRequirementType("have-pokedex-status", HavePokedexStatus::class.java)
            defineRequirementType("have-pokemon-in-party", HavePokemonInParty::class.java)
            defineRequirementType("have-pokemon-in-pc", HavePokemonInPC::class.java)
            defineRequirementType("have-pokemon-in-level-range", HavePokemonInLevelRange::class.java)
            defineRequirementType("have-money", HaveMoney::class.java)
            defineRequirementType("can-battle", CanBattle::class.java)
            defineRequirementType("is-riding-pokemon", IsRidingPokemon::class.java)

            defineActionType("give-pokemon", GivePokemon::class.java)
            defineActionType("take-pokemon", TakePokemon::class.java)
            defineActionType("change-money", ChangeMoney::class.java)
            defineActionType("show-choices", ShowChoices::class.java)
            defineActionType("spawn-and-battle", SpawnAndBattle::class.java)
            defineActionType("show-dialogue", ShowDialogue::class.java)
            defineActionType("heal-party", HealParty::class.java)
            defineActionType("apply-spec", ApplySpec::class.java)
            defineActionType("box-pokemon", BoxPokemon::class.java)

            defineModelAppendage(Waypoint::class.java)
        }

        LOGGER.info("Done!")
    }

    fun onConstruction(event: FMLCommonSetupEvent) {
        event.enqueueWork {
            getSimpleData<PixConfig>().also { it.save(uncache = false) }
            getSimpleData<PixMessages>().also { it.save(uncache = false) }

            PokemonSpecificationProxy.register(MarkRequirement(""))
            PokemonSpecificationProxy.register(UnmarkRequirement())

            UltimaQuestsMod.EVENT_BUS.register(WaypointListener)
            MinecraftForge.EVENT_BUS.register(WaypointListener)
            MinecraftForge.EVENT_BUS.register(InteractionListener)
            Pixelmon.EVENT_BUS.register(CaptureListener)
            Pixelmon.EVENT_BUS.register(BattleListener)
            Pixelmon.EVENT_BUS.register(EvolutionListener)
            Pixelmon.EVENT_BUS.register(HatchListener)
            Pixelmon.EVENT_BUS.register(FishingListener)
            Pixelmon.EVENT_BUS.register(LevelingListener)
            Pixelmon.EVENT_BUS.register(LearnsetListener)
            Pixelmon.EVENT_BUS.register(CookingListener)
            Pixelmon.EVENT_BUS.register(CameraListener)
            Pixelmon.EVENT_BUS.register(InteractionFixListener)
//            Pixelmon.EVENT_BUS.register(RaidListener)
        }
    }

    @SubscribeEvent
    fun on(event: RegisterCommandsEvent) {
        UltimaPixelmonCommand.register(event.dispatcher)
    }

    @SubscribeEvent
    fun on(event: ModelGsonEvent) {
        event.gsonBuilder.registerTypeAdapter(PokemonSpecification::class.java, PokemonSpecAdapter)
    }

    @SubscribeEvent
    fun on(event: StorageGsonEvent) {
        event.gsonBuilder.registerTypeAdapter(PokemonSpecification::class.java, PokemonSpecAdapter)
    }
}