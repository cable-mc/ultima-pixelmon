package com.cable.ultimapixelmon.listener

import com.cable.ultimapixelmon.model.tasks.LearnMove
import com.cable.ultimaquests.api.UltimaQuests
import com.pixelmonmod.pixelmon.api.events.pokemon.MovesetEvent
import net.minecraftforge.eventbus.api.SubscribeEvent

object LearnsetListener {
    @SubscribeEvent
    fun on(event: MovesetEvent.LearntMoveEvent) {
        event.pokemon.ownerPlayer?.let { player ->
            UltimaQuests.api.getTaskProcessor().processAllFor(
                clazz = LearnMove::class.java,
                data = event,
                player = player
            )
        }
    }
}