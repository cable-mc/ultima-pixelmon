package com.cable.ultimapixelmon.listener

import com.cable.ultimapixelmon.model.tasks.FishPokemon
import com.cable.ultimaquests.api.UltimaQuests
import com.pixelmonmod.pixelmon.api.events.FishingEvent
import com.pixelmonmod.pixelmon.entities.pixelmon.PixelmonEntity
import net.minecraftforge.eventbus.api.SubscribeEvent

object FishingListener {
    @SubscribeEvent
    fun on(event: FishingEvent.Reel) {
        val entity = event.optEntity.orElse(null) ?: return
        if (entity is PixelmonEntity) {
            UltimaQuests.api.getTaskProcessor().processAllFor(
                clazz = FishPokemon::class.java,
                data = event,
                player = event.player
            )
        }
    }
}