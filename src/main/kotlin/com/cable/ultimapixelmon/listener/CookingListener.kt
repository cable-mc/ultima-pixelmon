package com.cable.ultimapixelmon.listener

import com.cable.ultimapixelmon.model.tasks.CookCurry
import com.cable.ultimaquests.api.UltimaQuests
import com.pixelmonmod.pixelmon.api.events.CurryFinishedEvent
import net.minecraftforge.eventbus.api.SubscribeEvent

object CookingListener {
    @SubscribeEvent
    fun on(event: CurryFinishedEvent) {
        UltimaQuests.api.getTaskProcessor().processAllFor(
            clazz = CookCurry::class.java,
            data = event,
            player = event.player ?: return
        )
    }
}