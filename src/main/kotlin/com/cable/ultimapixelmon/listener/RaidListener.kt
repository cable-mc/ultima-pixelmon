package com.cable.ultimapixelmon.listener

import com.cable.ultimapixelmon.model.tasks.DefeatDen
import com.cable.ultimapixelmon.model.tasks.LoseToDen
import com.cable.ultimaquests.api.UltimaQuests
import com.cable.ultimaquests.internal.util.getOnlinePlayer
import com.pixelmonmod.pixelmon.api.events.raids.EndRaidEvent
import net.minecraftforge.eventbus.api.SubscribeEvent

object RaidListener {
    @SubscribeEvent
    fun on(event: EndRaidEvent) {
        val players = event.raid.players.mapNotNull { it.player?.getOnlinePlayer() }
        val succeeded = event.didRaidersWin()

        for (player in players) {
            if (succeeded) {
                UltimaQuests.api.getTaskProcessor().processAllFor(
                    clazz = DefeatDen::class.java,
                    data = event,
                    player = player
                )
            } else {
                UltimaQuests.api.getTaskProcessor().processAllFor(
                    clazz = LoseToDen::class.java,
                    data = event,
                    player = player
                )
            }
        }
    }
}