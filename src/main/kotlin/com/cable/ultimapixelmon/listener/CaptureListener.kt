package com.cable.ultimapixelmon.listener

import com.cable.ultimapixelmon.model.tasks.CatchPokemon
import com.cable.ultimaquests.api.UltimaQuests
import com.pixelmonmod.pixelmon.api.events.CaptureEvent
import net.minecraftforge.eventbus.api.SubscribeEvent

object CaptureListener {
    @SubscribeEvent
    fun on(event: CaptureEvent.SuccessfulCapture) {
        UltimaQuests.api.getTaskProcessor().processAllFor(
            clazz = CatchPokemon::class.java,
            data = event,
            player = event.player
        )
    }
}