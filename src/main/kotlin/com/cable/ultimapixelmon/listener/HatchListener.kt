package com.cable.ultimapixelmon.listener

import com.cable.ultimapixelmon.model.tasks.HatchPokemon
import com.cable.ultimaquests.api.UltimaQuests
import com.pixelmonmod.pixelmon.api.events.EggHatchEvent
import net.minecraftforge.eventbus.api.SubscribeEvent

object HatchListener {
    @SubscribeEvent
    fun on(event: EggHatchEvent.Post) {
        val player = event.pokemon.ownerPlayer ?: return
        UltimaQuests.api.getTaskProcessor().processAllFor(
            clazz = HatchPokemon::class.java,
            data = event,
            player = player
        )
    }
}