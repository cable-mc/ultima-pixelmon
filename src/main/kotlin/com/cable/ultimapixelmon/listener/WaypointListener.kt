package com.cable.ultimapixelmon.listener

import com.cable.library.data.getData
import com.cable.library.data.getSimpleData
import com.cable.library.reflect.into
import com.cable.library.tasks.after
import com.cable.ultimapixelmon.WaypointUpdater
import com.cable.ultimapixelmon.appendage.Waypoint
import com.cable.ultimapixelmon.appendage.Waypoint.Companion.SELECTED_STORE
import com.cable.ultimapixelmon.appendage.Waypoint.Companion.SELECTED_TASK
import com.cable.ultimapixelmon.config.PixConfig
import com.cable.ultimaquests.api.UltimaQuests
import com.cable.ultimaquests.api.event.QuestStartedEvent
import com.cable.ultimaquests.api.event.StageTransferEvent
import com.cable.ultimaquests.api.event.TaskCompletedEvent
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.quest.Quest
import com.cable.ultimaquests.internal.storage.CommonQuestData
import java.util.UUID
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraftforge.event.TickEvent
import net.minecraftforge.event.entity.player.PlayerEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.LogicalSide
import net.minecraftforge.fml.server.ServerLifecycleHooks

object WaypointListener {
    val waypoints = mutableMapOf<UUID, Waypoint>()
    var tick = 0

    @SubscribeEvent
    fun on(event: PlayerEvent.PlayerLoggedInEvent) {
        if (event.player !is ServerPlayerEntity) {
            return
        }

        WaypointUpdater.getActiveWaypoint(event.player.uuid)?.into { waypoints[event.player.uuid] = it }
    }

    @SubscribeEvent
    fun on(event: TickEvent.ServerTickEvent) {
        if (event.phase == TickEvent.Phase.END || event.side == LogicalSide.CLIENT) {
            return;
        }

        val config = getSimpleData<PixConfig>()
        if (++tick >= config.secondsBetweenWaypointUpdates * 20) {
            tick = 0
            ServerLifecycleHooks.getCurrentServer().playerList.players.forEach { player ->
                val waypoint = waypoints[player.uuid] ?: return@forEach
                WaypointUpdater.updateWaypoint(player, waypoint)
            }
        }
    }

    @SubscribeEvent
    fun on(event: PlayerEvent.PlayerLoggedOutEvent) {
        waypoints.remove(event.player.uuid)
    }

    @SubscribeEvent
    fun on(event: TaskCompletedEvent) {
        val waypoint = waypoints[event.ctx.player.uuid] ?: return
        if (waypoint.task == event.task) {
            after(seconds = 1) {
                WaypointUpdater.getActiveWaypoint(event.ctx.player.uuid)?.let { waypoint ->
                    waypoints[event.ctx.player.uuid] = waypoint
                }
            }
        }
    }

    @SubscribeEvent
    fun on(event: StageTransferEvent) {
        after(seconds = 1) {
            searchAndTrackAutoTasks(event.ctx, event.newStage.getQuest())
        }
    }

    @SubscribeEvent
    fun on(event: QuestStartedEvent) {
        ServerLifecycleHooks.getCurrentServer().playerList.players
            .filter { event.store in UltimaQuests.api.getProgressStores(it.uuid) }
            .forEach { searchAndTrackAutoTasks(QuestContext(it, event.store), event.quest) }
    }

    private fun searchAndTrackAutoTasks(ctx: QuestContext, quest: Quest<*>) {
        val common = ctx.player.getData<CommonQuestData>()
        if (!quest.canSee(ctx.player, ctx.store.getProgressFor(quest), true)) {
            return
        }
        val tasks = ctx.store.getProgressFor(quest)?.remainingTasks?.filter { !it.key.isHidden } ?: return
        val waypoint = tasks.mapNotNull { it.key.getAppendage<Waypoint>() }.find { it.trackAutomatically } ?: return
        common.doAndSave {
            this.selectedQuest = waypoint.task.getQuest().name
            this.setProperty(SELECTED_STORE, ctx.store.getStoreProvider().getName())
            this.setProperty(SELECTED_TASK, waypoint.task.name)
            waypoints[ctx.player.uuid] = waypoint
        }
    }
}