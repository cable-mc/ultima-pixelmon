package com.cable.ultimapixelmon.listener

import com.cable.ultimapixelmon.model.tasks.EvolvePokemon
import com.cable.ultimaquests.api.UltimaQuests
import com.pixelmonmod.pixelmon.api.events.EvolveEvent
import net.minecraftforge.eventbus.api.SubscribeEvent

object EvolutionListener {
    @SubscribeEvent
    fun on(event: EvolveEvent.Post) {
        UltimaQuests.api.getTaskProcessor().processAllFor(
            clazz = EvolvePokemon::class.java,
            data = event,
            player = event.player
        )
    }
}