package com.cable.ultimapixelmon.listener

import com.cable.library.data.getSimpleData
import com.cable.library.player.sendMessage
import com.cable.library.text.text
import com.cable.ultimapixelmon.battles.WatchedBattleController
import com.cable.ultimapixelmon.command.admin.getIsQuestTrainer
import com.cable.ultimapixelmon.config.PixMessages
import com.cable.ultimapixelmon.model.tasks.DefeatTrainer
import com.cable.ultimapixelmon.model.tasks.DefeatWildPokemon
import com.cable.ultimapixelmon.model.tasks.LoseToTrainer
import com.cable.ultimapixelmon.model.tasks.LoseToWildPokemon
import com.cable.ultimapixelmon.model.tasks.ReceiveItemDrop
import com.cable.ultimapixelmon.model.tasks.RunOutOfPokemon
import com.cable.ultimapixelmon.util.getParty
import com.cable.ultimaquests.api.UltimaQuests
import com.cable.ultimaquests.internal.util.getMark
import com.pixelmonmod.pixelmon.api.battles.BattleResults
import com.pixelmonmod.pixelmon.api.enums.BattleEndTaskType
import com.pixelmonmod.pixelmon.api.events.BeatTrainerEvent
import com.pixelmonmod.pixelmon.api.events.BeatWildPixelmonEvent
import com.pixelmonmod.pixelmon.api.events.DropEvent
import com.pixelmonmod.pixelmon.api.events.LostToTrainerEvent
import com.pixelmonmod.pixelmon.api.events.LostToWildPixelmonEvent
import com.pixelmonmod.pixelmon.api.events.PixelmonFaintEvent
import com.pixelmonmod.pixelmon.api.events.battles.BattleEndEvent
import com.pixelmonmod.pixelmon.api.events.battles.BattleStartedEvent
import com.pixelmonmod.pixelmon.api.events.battles.ForceEndBattleEvent
import com.pixelmonmod.pixelmon.battles.controller.participants.PlayerParticipant
import com.pixelmonmod.pixelmon.battles.controller.participants.TrainerParticipant
import com.pixelmonmod.pixelmon.entities.npcs.NPCTrainer
import com.pixelmonmod.pixelmon.entities.pixelmon.PixelmonEntity
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraftforge.eventbus.api.SubscribeEvent

object BattleListener {
    @SubscribeEvent
    fun on(event: BeatWildPixelmonEvent) {
        if (event.player == null) {
            return
        }

        event.player.getParty().addTaskForBattleEnd(BattleEndTaskType.ALWAYS_QUEUE) {
            UltimaQuests.api.getTaskProcessor().processAllFor(
                clazz = DefeatWildPokemon::class.java,
                data = event.wpp.allPokemon[0].pokemon,
                player = event.player
            )
        }
    }
    
    @SubscribeEvent
    fun on(event: BeatTrainerEvent) {
        if (event.player == null) {
            return
        }

        event.player.getParty().addTaskForBattleEnd(BattleEndTaskType.ALWAYS_QUEUE) {
            UltimaQuests.api.getTaskProcessor().processAllFor(
                clazz = DefeatTrainer::class.java,
                data = event,
                player = event.player
            )
        }
    }

    @SubscribeEvent
    fun on(event: LostToTrainerEvent) {
        if (event.player == null) {
            return
        }

        event.player.getParty().addTaskForBattleEnd(BattleEndTaskType.ALWAYS_QUEUE) {
            UltimaQuests.api.getTaskProcessor().processAllFor(
                clazz = LoseToTrainer::class.java,
                data = event,
                player = event.player
            )
        }
    }

    @SubscribeEvent
    fun on(event: LostToWildPixelmonEvent) {
        if (event.player == null) {
            return
        }

        val pokemon = (event.wpp.entity as PixelmonEntity).pokemon

        event.player.getParty().addTaskForBattleEnd(BattleEndTaskType.ALWAYS_QUEUE) {
            UltimaQuests.api.getTaskProcessor().processAllFor(
                clazz = LoseToWildPokemon::class.java,
                data = pokemon,
                player = event.player
            )
        }
    }

    @SubscribeEvent
    fun on(event: PixelmonFaintEvent.Post) {
        val player = event.player ?: return

        if (player.getParty().countAblePokemon() == 0) {
            UltimaQuests.api.getTaskProcessor().processAllFor(
                clazz = RunOutOfPokemon::class.java,
                player = player
            )
        }
    }

    @SubscribeEvent
    fun on(event: BattleEndEvent) {
        val bc = event.battleController
        if (bc is WatchedBattleController) {
            val player = bc.ctx.player
            val participant = bc.getParticipantForEntity(bc.ctx.player) ?: return
            val result = event.results[participant] ?: return

            player.getParty().addTaskForBattleEnd(BattleEndTaskType.ALWAYS_QUEUE) {
                with(bc) {
                    when (result) {
                        BattleResults.VICTORY -> tryConclude(onVictory)
                        BattleResults.DEFEAT -> tryConclude(onDefeat)
                        BattleResults.DRAW -> tryConclude(onDraw)
                        BattleResults.FLEE -> tryConclude(onFlee)
                    }
                }
            }
        }
    }

    @SubscribeEvent
    fun on(event: ForceEndBattleEvent) {
        val bc = event.battleController
        if (bc is WatchedBattleController) {
            event.players.forEach {
                it.getParty().addTaskForBattleEnd(BattleEndTaskType.ALWAYS_QUEUE) {
                    bc.tryConclude(bc.onError)
                }
            }
        }
    }

    @SubscribeEvent
    fun on(event: DropEvent) {
        UltimaQuests.api.getTaskProcessor().processAllFor(
            clazz = ReceiveItemDrop::class.java,
            data = event,
            player = event.player
        )
    }

    @SubscribeEvent
    fun on(event: BattleStartedEvent) {
        val allParticipants = event.teamOne.toList() + event.teamTwo.toList()
        val players = allParticipants.filterIsInstance<PlayerParticipant>().map { it.player }
        val trainers = allParticipants.filterIsInstance<TrainerParticipant>().map { it.trainer }

        for (trainer in trainers) {
            if (trainer.getIsQuestTrainer()) {
                for (player in players) {
                    if (!playerHasRelevantTask(player, trainer)) {
                        event.isCanceled = true
                        player.sendMessage(getSimpleData<PixMessages>().trainerIsQuestNPCMessage.text())
                        return
                    }
                }
            }
        }
    }

    private fun playerHasRelevantTask(player: ServerPlayerEntity, trainer: NPCTrainer): Boolean {
        val mark = trainer.getMark()
        val uuid = trainer.uuid

        val stores = UltimaQuests.api.getProgressStores(player.uuid)
        val currentDefeatTrainerTasks = stores.flatMap { it.getAllTasks(DefeatTrainer::class.java).map { it.first } }
        val currentLoseToTrainerTasks = stores.flatMap { it.getAllTasks(LoseToTrainer::class.java).map { it.first } }

        return currentDefeatTrainerTasks.any { it.mark == mark || it.uuid == uuid.toString() }
            || currentLoseToTrainerTasks.any { it.mark == mark || it.uuid == uuid.toString() }
    }
}