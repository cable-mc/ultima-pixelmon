package com.cable.ultimapixelmon.listener

import com.cable.ultimapixelmon.model.tasks.LevelUpPokemon
import com.cable.ultimaquests.api.UltimaQuests
import com.pixelmonmod.pixelmon.api.events.LevelUpEvent
import net.minecraftforge.eventbus.api.SubscribeEvent

object LevelingListener {
    @SubscribeEvent
    fun on(event: LevelUpEvent.Post) {
        val player = event.player ?: return
        UltimaQuests.api.getTaskProcessor().processAllFor(
            clazz = LevelUpPokemon::class.java,
            player = player,
            data = event
        )
    }
}