package com.cable.ultimapixelmon.listener

import com.pixelmonmod.pixelmon.api.events.npc.NPCEvent
import net.minecraft.util.Hand
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.entity.player.PlayerInteractEvent
import net.minecraftforge.eventbus.api.EventPriority
import net.minecraftforge.eventbus.api.SubscribeEvent

object InteractionFixListener {
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    fun on(event: NPCEvent.Interact) {
        val newEvent = PlayerInteractEvent.EntityInteract(event.player, Hand.MAIN_HAND, event.npc)
        if (MinecraftForge.EVENT_BUS.post(newEvent)) {
            event.isCanceled = true
        }
    }
}