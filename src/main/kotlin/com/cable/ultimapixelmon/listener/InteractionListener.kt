package com.cable.ultimapixelmon.listener

import com.cable.ultimapixelmon.model.tasks.ChatToEntity
import com.cable.ultimaquests.api.UltimaQuests
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.util.Hand
import net.minecraftforge.event.entity.player.PlayerInteractEvent
import net.minecraftforge.eventbus.api.EventPriority
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.LogicalSide

object InteractionListener {
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    fun on(event: PlayerInteractEvent.EntityInteract) {
        if (event.target !is LivingEntity || event.side == LogicalSide.CLIENT || event.hand != Hand.MAIN_HAND) {
            return;
        }

        UltimaQuests.api.getTaskProcessor().processAllFor(
            clazz = ChatToEntity::class.java,
            data = event,
            player = event.player as ServerPlayerEntity
        )
    }
}