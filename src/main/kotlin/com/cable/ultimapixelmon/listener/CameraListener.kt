package com.cable.ultimapixelmon.listener

import com.cable.ultimapixelmon.model.tasks.TakePhoto
import com.cable.ultimaquests.api.UltimaQuests
import com.pixelmonmod.pixelmon.api.events.CameraEvent
import net.minecraftforge.eventbus.api.SubscribeEvent

object CameraListener {
    @SubscribeEvent
    fun on(event: CameraEvent.TakePhoto) {
        UltimaQuests.api.getTaskProcessor().processAllFor(
            clazz = TakePhoto::class.java,
            data = event,
            player = event.player
        )
    }
}