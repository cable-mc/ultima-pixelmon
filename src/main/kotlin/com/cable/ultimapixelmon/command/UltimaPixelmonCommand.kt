package com.cable.ultimapixelmon.command

import com.cable.library.player.hasPermission
import com.cable.ultimapixelmon.command.admin.questTrainerCommand
import com.mojang.brigadier.CommandDispatcher
import net.minecraft.command.CommandSource
import net.minecraft.command.Commands

object UltimaPixelmonCommand {
    fun register(dispatcher: CommandDispatcher<CommandSource>) {
        val mainCommand = Commands.literal("ultimapixelmon")
            .requires { it.hasPermission("ultimapixelmon.user.command.ultimapixelmon") }
            .then(questTrainerCommand)
        // Other subcommands

        dispatcher.register(mainCommand)
        mainCommand.build().also {
            dispatcher.register(Commands.literal("up").redirect(it).executes(mainCommand.command))
        }
    }
}