package com.cable.ultimapixelmon.command.admin

import com.cable.library.player.executesSuccessfully
import com.cable.library.player.hasPermission
import com.cable.library.player.sendMessage
import com.cable.library.text.add
import com.cable.library.text.darkGreen
import com.cable.library.text.gray
import com.cable.library.text.red
import com.cable.ultimaquests.internal.util.traceEntity
import com.mojang.brigadier.arguments.StringArgumentType
import com.mojang.brigadier.context.CommandContext
import com.pixelmonmod.pixelmon.entities.npcs.NPCTrainer
import net.minecraft.command.CommandSource
import net.minecraft.command.Commands
import net.minecraft.entity.player.ServerPlayerEntity

private const val SET = "set"
private const val UNSET = "unset"
private const val CHECK = "check"
private const val QUEST_TRAINER_TAG = "isQuestTrainer"
fun NPCTrainer.setIsQuestTrainer() = persistentData.putBoolean(QUEST_TRAINER_TAG, true)
fun NPCTrainer.getIsQuestTrainer() = persistentData.contains(QUEST_TRAINER_TAG)
fun NPCTrainer.unsetIsQuestTrainer() = persistentData.remove(QUEST_TRAINER_TAG)

val questTrainerCommand = Commands.literal("questtrainer")
    .requires { it.playerOrException is ServerPlayerEntity }
    .requires { it.hasPermission("ultimapixelmon.admin.command.questtrainer") }
    .then(Commands.literal("set").executesSuccessfully { run(it, SET) })
    .then(Commands.literal("unset").executesSuccessfully { run(it, UNSET) })
    .then(Commands.literal("check").executesSuccessfully { run(it, CHECK) })

fun run(context: CommandContext<CommandSource>, option: String) {
    val player = context.source.playerOrException
    val lookingAt = traceEntity(player.getLevel(), player.getEyePosition(0F), player.lookAngle)
        ?: return player.sendMessage("You are not looking at an entity.".red())

    if (lookingAt is NPCTrainer) {
        val isQuestTrainer = lookingAt.getIsQuestTrainer()
        when (option) {
            CHECK -> player.sendMessage(
                if (isQuestTrainer) {
                    "That ".gray().add("is ".darkGreen()).add("a quest trainer.".gray())
                } else {
                    "That ".gray().add("is not ".red()).add("a quest trainer".gray())
                }
            )
            SET -> {
                if (isQuestTrainer) {
                    player.sendMessage("That NPC is already marked as a quest trainer.".red())
                } else {
                    lookingAt.setIsQuestTrainer()
                    player.sendMessage("Successfully marked as a quest trainer.".darkGreen())
                }
            }
            UNSET -> {
                if (isQuestTrainer) {
                    lookingAt.unsetIsQuestTrainer()
                    player.sendMessage("Successfully unmarked as a quest trainer.".darkGreen())
                } else {
                    player.sendMessage("That NPC isn't a quest trainer.".red())
                }
            }
        }
    } else {
        player.sendMessage("You are not looking at a trainer.".red())
    }
}