package com.cable.ultimapixelmon.battles

import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.action.Action
import com.pixelmonmod.pixelmon.battles.api.rules.BattleRules
import com.pixelmonmod.pixelmon.battles.controller.BattleController
import com.pixelmonmod.pixelmon.battles.controller.participants.BattleParticipant

class WatchedBattleController(
    val ctx: QuestContext,
    val onVictory: List<Action>,
    val onDefeat: List<Action>,
    val onDraw: List<Action>,
    val onFlee: List<Action>,
    val onError: List<Action>,
    team1: Array<BattleParticipant>,
    team2: Array<BattleParticipant>,
    rules: BattleRules
) : BattleController(team1, team2, rules) {

    var concluded = false

    fun tryConclude(actions: List<Action>) {
        if (!concluded) {
            actions.forEach { it.schedule(ctx) }
            concluded = true
        }
    }
}