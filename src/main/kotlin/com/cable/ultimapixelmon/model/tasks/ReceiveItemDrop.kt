package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.editor.EmbeddedField
import com.cable.ultimaquests.api.editor.NullableEnumField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.quest.Quest
import com.cable.ultimaquests.api.model.quest.Stage
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.cable.ultimaquests.internal.model.ItemField
import com.pixelmonmod.pixelmon.api.events.DropEvent
import com.pixelmonmod.pixelmon.comm.packetHandlers.itemDrops.ItemDropMode
import com.pixelmonmod.pixelmon.entities.pixelmon.PixelmonEntity

class ReceiveItemDrop: SimpleCountableTask<DropEvent>() {
    override fun getDataClass() = DropEvent::class.java

    enum class DropCreature {
        POKEMON,
        TRAINER
    }

    @EditorField(displayLabel = "Drop Mode", description = "The situation the drop needs to be from, if any.")
    @NullableEnumField
    val dropMode: ItemDropMode? = null
    @EditorField(displayLabel = "Creature Type", description = "The type of creature that must have dropped it, if any.")
    @NullableEnumField
    val dropCreatureType: DropCreature? = null
    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon Spec of the Pokémon that must drop this item. This will not apply to trainer dropped items.", type = "string")
    val spec = BLANK_SPEC
    @EmbeddedField
    var itemToGet = ItemField()

    override fun sanitize(quest: Quest<*>, stage: Stage?) {
        super.sanitize(quest, stage)
        itemToGet.sanitize()
    }

    override fun validate() {
        super.validate()
        itemToGet.validate()
    }

    override fun affectProgress(data: DropEvent, progress: CountableTaskProgress, ctx: QuestContext) {
        if (dropMode != null && dropMode != data.dropMode) {
            return
        } else if (dropCreatureType != null) {
            if (dropCreatureType == DropCreature.POKEMON && !data.isPokemon) {
                return
            } else if (dropCreatureType == DropCreature.TRAINER && !data.isTrainer) {
                return
            }
        }

        if (data.entity is PixelmonEntity && !spec.matches(data.entity as PixelmonEntity)) {
            return
        }

        data.drops.forEach { drop ->
            if (this.itemToGet.matches(ctx.getItemRegistry(), drop.item)) {
                progress.times(drop.item.count)
            }
        }
    }
}