package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.pixelmonmod.pixelmon.api.events.FishingEvent
import com.pixelmonmod.pixelmon.entities.pixelmon.PixelmonEntity

class FishPokemon : SimpleCountableTask<FishingEvent.Reel>() {
    override fun getDataClass() = FishingEvent.Reel::class.java

    @EditorField(displayLabel = "Fished Pokémon Spec", description = "The Pokémon Spec that the caught Pokémon must fit. It can be blank.", type = "string")
    val spec = BLANK_SPEC

    override fun affectProgress(data: FishingEvent.Reel, progress: CountableTaskProgress, ctx: QuestContext) {
        val pixelmon = data.optEntity.orElse(null) as? PixelmonEntity ?: return
        if (spec.matches(pixelmon)) {
            super.affectProgress(data, progress, ctx)
        }
    }
}