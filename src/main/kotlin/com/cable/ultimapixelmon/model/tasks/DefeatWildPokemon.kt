package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon

open class DefeatWildPokemon : SimpleCountableTask<Pokemon>() {
    override fun getDataClass() = Pokemon::class.java

    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon to defeat, as a Pokémon Spec such as pikachu lvl:15 shiny", type = "string")
    var spec = BLANK_SPEC

    override fun affectProgress(data: Pokemon, progress: CountableTaskProgress, ctx: QuestContext) {
        if (spec.matches(data)) {
            super.affectProgress(data, progress, ctx)
        }
    }
}