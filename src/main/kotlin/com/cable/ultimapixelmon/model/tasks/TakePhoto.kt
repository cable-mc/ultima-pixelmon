package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.quest.Quest
import com.cable.ultimaquests.api.model.quest.Stage
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.cable.ultimaquests.api.util.sanitize
import com.cable.ultimaquests.internal.util.setMark
import com.pixelmonmod.pixelmon.api.events.CameraEvent

class TakePhoto : SimpleCountableTask<CameraEvent.TakePhoto>() {
    override fun getDataClass() = CameraEvent.TakePhoto::class.java

    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon Spec describing the Pokémon they must take a photo of. This can be blank.", type = "string")
    val spec = BLANK_SPEC
    @EditorField(displayLabel = "Mark", description = "The mark to apply to the item they get from taking the photo, if any.")
    var mark = ""
    @EditorField(displayLabel = "Ownership", description = "Which Pokémon count with respect to their ownership status.")
    var pokemonOwnership = PokemonOwnership.ANY

    enum class PokemonOwnership {
        WILD,
        OWNED_BY_SELF,
        OWNED_BY_PLAYER,
        OWNED,
        ANY
    }

    override fun sanitize(quest: Quest<*>, stage: Stage?) {
        super.sanitize(quest, stage)
        mark = mark.sanitize()
    }

    override fun affectProgress(data: CameraEvent.TakePhoto, progress: CountableTaskProgress, ctx: QuestContext) {
        if (!spec.matches(data.pixelmon)) {
            return
        }

        if (mark.isNotBlank()) {
            data.photo.setMark(mark)
        }

        if (pokemonOwnership == PokemonOwnership.WILD && data.pixelmon.owner != null) {
            return
        } else if (pokemonOwnership == PokemonOwnership.OWNED && data.pixelmon.owner == null) {
            return
        } else if (pokemonOwnership == PokemonOwnership.OWNED_BY_SELF && data.pixelmon.pokemon.ownerPlayerUUID != ctx.player.uuid) {
            return
        } else if (pokemonOwnership == PokemonOwnership.OWNED_BY_PLAYER && data.pixelmon.pokemon.ownerPlayerUUID == null) {
            return
        }

        super.affectProgress(data, progress, ctx)
    }
}