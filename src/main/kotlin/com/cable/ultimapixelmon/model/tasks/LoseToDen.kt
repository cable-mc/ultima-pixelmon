package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.pixelmonmod.pixelmon.api.events.raids.EndRaidEvent

class LoseToDen : SimpleCountableTask<EndRaidEvent>() {
    override fun getDataClass() = EndRaidEvent::class.java
}