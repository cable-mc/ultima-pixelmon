package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.cable.ultimaquests.api.util.sanitize
import com.pixelmonmod.pixelmon.api.events.pokemon.MovesetEvent.LearntMoveEvent

class LearnMove : SimpleCountableTask<LearntMoveEvent>() {
    override fun getDataClass() = LearntMoveEvent::class.java

    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon Spec describing the Pokémon learning the move. If blank, any Pokémon will do.", type = "string")
    val spec = BLANK_SPEC
    @EditorField(displayLabel = "Learned Move Name", description = "The English name of the move learned by the Pokémon. If blank, any move will do.")
    val learnedMove = ""
    @EditorField(displayLabel = "Replaced Move Name", description = "The English name of the move replaced when learning the new move. If blank, no need for a move to be replaced.")
    val replacedMove = ""

    override fun affectProgress(data: LearntMoveEvent, progress: CountableTaskProgress, ctx: QuestContext) {
        if (learnedMove.isNotBlank() && learnedMove.sanitize() != data.learntAttack.move.attackName.sanitize()) {
            return
        } else if (replacedMove.isNotBlank() && replacedMove.sanitize() != data.replacedAttack?.move?.attackName?.sanitize()) {
            return
        } else if (!spec.matches(data.pokemon)) {
            return
        }

        super.affectProgress(data, progress, ctx)
    }
}