package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimaquests.api.model.task.SimpleCountableTask

class RunOutOfPokemon : SimpleCountableTask<Object>() {
    override fun getDataClass(): Class<Object> = Object::class.java
}