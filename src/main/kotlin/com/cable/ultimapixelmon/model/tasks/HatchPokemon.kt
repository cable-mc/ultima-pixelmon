package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.pixelmonmod.pixelmon.api.events.EggHatchEvent

class HatchPokemon : SimpleCountableTask<EggHatchEvent>() {
    override fun getDataClass() = EggHatchEvent::class.java

    @EditorField(displayLabel = "Pokémon Spec", type = "string", description = "The Pokémon Spec that the hatched Pokémon must fit. Can be blank.")
    val spec = BLANK_SPEC

    override fun affectProgress(data: EggHatchEvent, progress: CountableTaskProgress, ctx: QuestContext) {
        if (spec.matches(data.pokemon)) {
            super.affectProgress(data, progress, ctx)
        }
    }
}