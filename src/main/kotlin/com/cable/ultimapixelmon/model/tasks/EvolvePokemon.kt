package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimapixelmon.util.isBlank
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.pixelmonmod.pixelmon.api.events.EvolveEvent

class EvolvePokemon : SimpleCountableTask<EvolveEvent.Post>() {
    override fun getDataClass() = EvolveEvent.Post::class.java

    @EditorField(displayLabel = "Evolve Into Pokémon Spec", type = "string", description = "The Pokémon it must evolve into. It can be blank.")
    val toPokemon = BLANK_SPEC

    override fun affectProgress(data: EvolveEvent.Post, progress: CountableTaskProgress, ctx: QuestContext) {
        if (!toPokemon.isBlank() && !toPokemon.matches(data.pokemon)) {
            return
        } else {
            super.affectProgress(data, progress, ctx)
        }
    }
}