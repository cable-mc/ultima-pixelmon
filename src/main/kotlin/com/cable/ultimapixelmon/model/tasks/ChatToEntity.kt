package com.cable.ultimapixelmon.model.tasks

import com.cable.library.event.subscribeOnce
import com.cable.ultimapixelmon.util.momentarily
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.quest.Quest
import com.cable.ultimaquests.api.model.quest.Stage
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.cable.ultimaquests.api.model.validationResults
import com.cable.ultimaquests.api.util.sanitize
import com.cable.ultimaquests.internal.checkTaskIsCompleted
import com.cable.ultimaquests.internal.util.getMark
import com.pixelmonmod.pixelmon.Pixelmon
import com.pixelmonmod.pixelmon.api.dialogue.Dialogue
import com.pixelmonmod.pixelmon.api.events.dialogue.DialogueEndedEvent
import java.util.UUID
import net.minecraft.entity.LivingEntity
import net.minecraftforge.event.entity.player.PlayerInteractEvent

open class ChatToEntity : SimpleCountableTask<PlayerInteractEvent.EntityInteract>() {
    override fun getDataClass(): Class<PlayerInteractEvent.EntityInteract> = PlayerInteractEvent.EntityInteract::class.java

    @EditorField(displayLabel = "Mark", description = "The mark required on the entity they need to interact with.")
    var mark = ""
    @EditorField(displayLabel = "UUID", description = "The UUID of the entity they need to interact with. Probably easier to use the mark option instead. Defaults to blank.")
    var uuid = ""
    @EditorField(displayLabel = "Chat Name", description = "The name to appear in the dialogue box's name slot. Defaults to blank.")
    var chatName = ""
    @EditorField(displayLabel = "ESC Closes GUI", description = "If true, pressing ESC will close the GUI and bypass some actions. Defaults to false, please never set this to true unless you know what you're doing.")
    var escapeClosesGUI = false
    @EditorField(displayLabel = "Chat Pages", description = "The pages to appear in the dialogue box.", type = "string")
    var chatPages = mutableListOf<String>()

    override fun sanitize(quest: Quest<*>, stage: Stage?) {
        super.sanitize(quest, stage)
        mark.sanitize()
        uuid = uuid.trim()
    }

    override fun validate() {
        super.validate()

        if (mark.isBlank() && uuid.isBlank()) {
            validationResults.addFailure("Both mark and UUID are blank. At least one of these must have a value.")
        }
        if (uuid.isNotBlank()) {
            try {
                UUID.fromString(uuid)
            } catch (e: NumberFormatException) {
                validationResults.addFailure("Invalid UUID entered.")
            }
        }
        if (chatPages.isEmpty()) {
            validationResults.addFailure("There are no chat pages. Seems a bit useless.")
        }
    }

    override fun affectProgress(data: PlayerInteractEvent.EntityInteract, progress: CountableTaskProgress, ctx: QuestContext) {
        if (data.target !is LivingEntity) {
            return;
        }

        val entity = data.target as LivingEntity
        if (mark.isNotBlank() && mark != entity.getMark()) {
            return;
        }

        if (uuid.isNotBlank() && UUID.fromString(uuid) != entity.uuid) {
            return;
        }

        val dialogues = chatPages.map {
            Dialogue.builder()
                .setName(ctx.substituteInto(chatName))
                .setText(ctx.substituteInto(it))
                .also { builder ->
                    if (escapeClosesGUI) {
                        builder.escapeCloses()
                    } else {
                        builder.requireManualClose()
                    }
                }
                .build()
        }

        subscribeOnce<DialogueEndedEvent>(
            eventBus = Pixelmon.EVENT_BUS,
            condition = { it.player == ctx.player },
            handler = {
                momentarily {
                    super.affectProgress(data, progress, ctx)
                    checkTaskIsCompleted(ctx, progress)
                }
            }
        )

        data.isCanceled = true
        Dialogue.setPlayerDialogueData(ctx.player, dialogues, true)
    }
}