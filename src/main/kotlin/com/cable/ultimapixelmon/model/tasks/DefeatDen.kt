package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.pixelmonmod.pixelmon.api.events.raids.EndRaidEvent

class DefeatDen : SimpleCountableTask<EndRaidEvent>() {
//    @EditorField(displayLabel = "Dynamaxed Pokémon", description = "A Pokémon Spec describing the Pokémon that was being battled at the den.")
//    var pokemon = BLANK_SPEC
    override fun getDataClass() = EndRaidEvent::class.java
}