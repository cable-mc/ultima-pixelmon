package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimapixelmon.model.actions.ChoiceData
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.quest.Quest
import com.cable.ultimaquests.api.model.quest.Stage
import com.cable.ultimaquests.api.model.task.BasicTaskProgress
import com.cable.ultimaquests.api.model.task.Task
import com.cable.ultimaquests.api.model.validationResults
import com.cable.ultimaquests.api.util.sanitize

open class SelectChoice : Task<BasicTaskProgress, ChoiceData>() {
    override fun getDataClass() = ChoiceData::class.java
    override fun getProgressClass() = BasicTaskProgress::class.java

    @EditorField(displayLabel = "Choice ID", description = "The choice ID that was used in show-choices.")
    var choiceID = ""
    @EditorField(displayLabel = "Choice Number", description = "The choice number they need to make. The first choice is choice number 1.")
    var choiceNumber = 1

    override fun sanitize(quest: Quest<*>, stage: Stage?) {
        super.sanitize(quest, stage)
        choiceID.sanitize()
    }

    override fun validate() {
        super.validate()
        if (choiceID.isBlank()) {
            validationResults.addFailure("The choice ID is blank. It shouldn't be.")
        }
        if (choiceNumber <= 0) {
            validationResults.addFailure("Choice number is less than or equal to zero. It must be at least 1, as choices start from number 1.")
        }
    }

    override fun affectProgress(data: ChoiceData, progress: BasicTaskProgress, ctx: QuestContext) {
        if (choiceID == data.choiceID && choiceNumber == data.choiceNumber) {
            progress.complete()
        }
    }
}