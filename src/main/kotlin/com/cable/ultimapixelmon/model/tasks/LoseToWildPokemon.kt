package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimapixelmon.UltimaPixelmon
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon

class LoseToWildPokemon : SimpleCountableTask<Pokemon>() {
    override fun getDataClass(): Class<Pokemon>  = Pokemon::class.java

    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon to lose to, as a Pokémon Spec such as pikachu lvl:15 shiny", type = "string")
    var spec = UltimaPixelmon.BLANK_SPEC

    override fun affectProgress(data: Pokemon, progress: CountableTaskProgress, ctx: QuestContext) {
        if (spec.matches(data)) {
            super.affectProgress(data, progress, ctx)
        }
    }
}