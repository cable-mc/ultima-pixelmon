package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.pixelmonmod.api.pokemon.PokemonSpecification
import com.pixelmonmod.pixelmon.api.events.CaptureEvent

open class CatchPokemon : SimpleCountableTask<CaptureEvent.SuccessfulCapture>() {
    override fun getDataClass() = CaptureEvent.SuccessfulCapture::class.java

    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon to catch as a space-separated Pokémon Spec such as 'pikachu lvl:15'", type = "string")
    var spec: PokemonSpecification = BLANK_SPEC

    override fun affectProgress(data: CaptureEvent.SuccessfulCapture, progress: CountableTaskProgress, ctx: QuestContext) {
        if (spec.matches(data.pokemon)) {
            super.affectProgress(data, progress, ctx)
        }
    }
}