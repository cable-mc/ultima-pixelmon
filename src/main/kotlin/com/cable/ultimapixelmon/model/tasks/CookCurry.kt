package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.editor.NullableEnumField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.pixelmonmod.pixelmon.api.events.CurryFinishedEvent
import com.pixelmonmod.pixelmon.enums.EnumBerryFlavor
import com.pixelmonmod.pixelmon.enums.EnumCurryKey
import com.pixelmonmod.pixelmon.enums.EnumCurryRating

class CookCurry : SimpleCountableTask<CurryFinishedEvent>() {
    override fun getDataClass() = CurryFinishedEvent::class.java

    @EditorField(displayLabel = "Curry Type", description = "The type of curry it must be.")
    @NullableEnumField
    val curryType: EnumCurryKey? = null
    @EditorField(displayLabel = "Rating", description = "The rating required on the curry.")
    @NullableEnumField
    val rating: EnumCurryRating? = null
    @EditorField(displayLabel = "Flavour", description = "The flavour required on the curry.")
    @NullableEnumField
    val flavour: EnumBerryFlavor? = null

    override fun affectProgress(data: CurryFinishedEvent, progress: CountableTaskProgress, ctx: QuestContext) {
        if (curryType != null && data.curryKey != curryType) {
            return
        } else if (rating != null && data.rating != rating) {
            return
        } else if (flavour != null && data.cookingFlavor != flavour) {
            return
        }

        super.affectProgress(data, progress, ctx)
    }
}