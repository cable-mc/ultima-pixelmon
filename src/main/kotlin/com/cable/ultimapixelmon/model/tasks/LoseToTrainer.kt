package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.quest.Quest
import com.cable.ultimaquests.api.model.quest.Stage
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.cable.ultimaquests.api.model.validationResults
import com.cable.ultimaquests.api.util.sanitize
import com.cable.ultimaquests.internal.util.getMark
import com.pixelmonmod.pixelmon.api.events.LostToTrainerEvent
import java.util.UUID

class LoseToTrainer : SimpleCountableTask<LostToTrainerEvent>() {
    override fun getDataClass() = LostToTrainerEvent::class.java

    @EditorField(displayLabel = "Mark", description = "The mark required on the trainer they need to lose to.")
    var mark = ""
    @EditorField(displayLabel = "UUID", description = "The UUID of the trainer they need to lose to. Probably easier to use the mark option instead. Defaults to blank.")
    var uuid = ""

    override fun sanitize(quest: Quest<*>, stage: Stage?) {
        super.sanitize(quest, stage)
        mark = mark.sanitize()
    }

    override fun validate() {
        super.validate()
        if (uuid.isNotBlank() && uuid.isNotEmpty()) {
            try {
                UUID.fromString(uuid)
            } catch (nfe: NumberFormatException) {
                validationResults.addFailure("UUID provided is invalid: $uuid")
            }
        }
    }

    override fun affectProgress(data: LostToTrainerEvent, progress: CountableTaskProgress, ctx: QuestContext) {
        if (data.trainer.uuid.toString() == uuid || data.trainer.getMark() == mark || (uuid.isBlank() && mark.isBlank())) {
            super.affectProgress(data, progress, ctx)
        }
    }
}