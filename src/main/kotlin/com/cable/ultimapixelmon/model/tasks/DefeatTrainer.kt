package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.quest.Quest
import com.cable.ultimaquests.api.model.quest.Stage
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.cable.ultimaquests.api.model.validationResults
import com.cable.ultimaquests.api.util.sanitize
import com.cable.ultimaquests.internal.util.getMark
import com.pixelmonmod.pixelmon.api.events.BeatTrainerEvent
import java.util.UUID

class DefeatTrainer : SimpleCountableTask<BeatTrainerEvent>() {
    override fun getDataClass() = BeatTrainerEvent::class.java

    @EditorField(displayLabel = "Mark", description = "The mark required on the trainer they need to beat.")
    var mark = ""
    @EditorField(displayLabel = "UUID", description = "The UUID of the trainer they need to beat. Probably easier to use the mark option instead. Defaults to blank.")
    var uuid = ""
    @EditorField(displayLabel = "Minimum Level", description = "The minimum level of the trainer that will count. Defaults to zero.")
    var minimumLevel = 0
    @EditorField(displayLabel = "Maximum Level", description = "The maximum level of the trainer that will count. Defaults to infinity.")
    var maximumLevel = Int.MAX_VALUE

    override fun sanitize(quest: Quest<*>, stage: Stage?) {
        super.sanitize(quest, stage)
        mark = mark.sanitize()
    }

    override fun validate() {
        super.validate()
        if (uuid.isNotBlank() && uuid.isNotEmpty()) {
            try {
                UUID.fromString(uuid)
            } catch (nfe: NumberFormatException) {
                validationResults.addFailure("UUID provided is invalid: $uuid")
            }
        }
        if (minimumLevel > maximumLevel) {
            validationResults.addFailure("Minimum level is higher than the maximum level -  this task cannot be completed.")
        }
    }

    override fun affectProgress(data: BeatTrainerEvent, progress: CountableTaskProgress, ctx: QuestContext) {
        if (data.trainer.uuid.toString() == uuid || data.trainer.getMark() == mark || (uuid.isBlank() && mark.isBlank())) {
            if (data.trainer.trainerLevel in minimumLevel..maximumLevel) {
                super.affectProgress(data, progress, ctx)
            }
        }
    }
}