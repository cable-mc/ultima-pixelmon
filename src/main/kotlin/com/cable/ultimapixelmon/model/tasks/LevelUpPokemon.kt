package com.cable.ultimapixelmon.model.tasks

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.task.CountableTaskProgress
import com.cable.ultimaquests.api.model.task.SimpleCountableTask
import com.pixelmonmod.pixelmon.api.events.LevelUpEvent

class LevelUpPokemon : SimpleCountableTask<LevelUpEvent>() {
    override fun getDataClass() = LevelUpEvent::class.java

    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon Spec condition on the Pokémon leveling up. If you wanted it to be any Pokémon reaching level 10, this could just be 'lvl:10'.", type = "string")
    val spec = BLANK_SPEC
    @EditorField(displayLabel = "Level", description = "The level that the Pokémon must be at or above. If left at -1, there is no specific level they need to be passing.")
    val level = -1

    override fun affectProgress(data: LevelUpEvent, progress: CountableTaskProgress, ctx: QuestContext) {
        if (spec.matches(data.pokemon)) {
            if (level != -1) {
                if (data.pokemon.pokemonLevel >= level || data.afterLevel < level) {
                    return
                }
            }
            super.affectProgress(data, progress, ctx)
        }
    }
}