package com.cable.ultimapixelmon.model.requirements

import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.requirement.Requirement
import com.cable.ultimaquests.api.model.validationResults
import com.pixelmonmod.pixelmon.api.pokedex.PokedexRegistrationStatus
import com.pixelmonmod.pixelmon.api.pokemon.species.Pokedex
import com.pixelmonmod.pixelmon.api.storage.StorageProxy

class HavePokedexCount : Requirement() {
    @EditorField(displayLabel = "Count", description = "The minimum amount they need to have on or above the specific status")
    val count = 0
    @EditorField(displayLabel = "Status", description = "The minimum status they need to have on a Pokémon for it to count.")
    val status = PokedexRegistrationStatus.CAUGHT

    override fun validate() {
        super.validate()
        if (count < 0) {
            validationResults.addFailure("Count should be positive.")
        }
    }

    override fun passes(ctx: QuestContext): Boolean {
        val pokedex = StorageProxy.getParty(ctx.player).playerPokedex
        val amount = when(status) {
            PokedexRegistrationStatus.CAUGHT -> pokedex.countCaught()
            PokedexRegistrationStatus.SEEN -> pokedex.countSeen()
            PokedexRegistrationStatus.UNKNOWN -> Pokedex.pokedexSize - pokedex.countCaught() - pokedex.countSeen()
        }
        return amount >= count
    }
}