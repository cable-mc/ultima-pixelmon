package com.cable.ultimapixelmon.model.requirements

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.requirement.Requirement
import com.pixelmonmod.pixelmon.entities.pixelmon.PixelmonEntity

class IsRidingPokemon : Requirement() {
    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon Spec describing the type of Pokémon they need to be riding. If it's blank, then it doesn't matter what Pokémon.", type = "string")
    var spec = BLANK_SPEC

    override fun passes(ctx: QuestContext): Boolean {
        val riddenEntity = ctx.player.vehicle ?: return false
        return riddenEntity is PixelmonEntity && spec.matches(riddenEntity)
    }
}