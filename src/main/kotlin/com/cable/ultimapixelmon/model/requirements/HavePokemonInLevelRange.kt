package com.cable.ultimapixelmon.model.requirements

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.requirement.Requirement
import com.cable.ultimaquests.api.model.validationResults
import com.pixelmonmod.pixelmon.api.storage.StorageProxy

class HavePokemonInLevelRange : Requirement() {
    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon Spec that a Pokémon must match as well as the level constraint. This can be blank.", type = "string")
    val spec = BLANK_SPEC
    @EditorField(displayLabel = "Minimum Level", description = "The minimum level a Pokémon in the party can be.")
    val minLevel = 1
    @EditorField(displayLabel = "Maximum Level", description = "The maximum level a Pokémon in the party can be.")
    val maxLevel = 100

    override fun validate() {
        super.validate()
        if (minLevel > maxLevel) {
            validationResults.addFailure("Minimum Level is larger than Maximum Level... someone's an idiot.")
        }
    }

    override fun passes(ctx: QuestContext): Boolean {
        val party = StorageProxy.getParty(ctx.player.uuid)
        return party.team.any { it.pokemonLevel in minLevel..maxLevel && spec.matches(it) }
    }
}