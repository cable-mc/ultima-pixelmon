package com.cable.ultimapixelmon.model.requirements

import com.cable.ultimapixelmon.UltimaPixelmon
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.requirement.Requirement
import com.cable.ultimaquests.api.model.validationResults
import com.pixelmonmod.pixelmon.api.storage.StorageProxy

open class HavePokemonInPC : Requirement() {
    @EditorField(displayLabel = "Pokémon", description = "The Pokémon to have as a space-separated Pokémon Spec such as pikachu lvl:15 shiny", type = "string")
    val spec = UltimaPixelmon.BLANK_SPEC
    @EditorField(displayLabel = "Box", description = "The box the Pokémon is meant to be in. -1 means it doesn't matter, and is the default.")
    val box = -1
    @EditorField(displayLabel = "Slot", description = "The slot that the Pokémon must be in, 1 up to 30. -1 means it doesn't matter, and is the default.")
    val slot = -1

    override fun validate() {
        super.validate()
        if (box != -1 && box < 1) {
            validationResults.addFailure("Box should be either -1 or larger than zero")
        }
        if (slot != -1 && (slot < 1 || slot > 30)) {
            validationResults.addFailure("Slot should be between 1 and 30, or be -1.")
        }
    }

    override fun passes(ctx: QuestContext): Boolean {
        val pc = StorageProxy.getPCForPlayer(ctx.player)
        val boxesToCheck = if (box == -1) pc.boxes.toList() else listOf(pc.boxes[box - 1])
        val slotsToCheck = if (slot == -1) 0 until 30 else listOf(slot - 1)
        for (box in boxesToCheck) {
            slot@
            for (slot in slotsToCheck) {
                val pokemon = box.get(slot) ?: continue@slot
                if (this.spec.matches(pokemon)) {
                    return true
                }
            }
        }
        return false
    }
}