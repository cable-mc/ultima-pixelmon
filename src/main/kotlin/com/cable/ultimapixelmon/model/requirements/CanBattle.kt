package com.cable.ultimapixelmon.model.requirements

import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.requirement.Requirement
import com.pixelmonmod.pixelmon.api.storage.StorageProxy

class CanBattle : Requirement() {
    @EditorField(displayLabel = "Necessary Pokémon", description = "The number of Pokémon that need to be able to battle, at a minimum. Defaults to 1.")
    val necessaryPokemon = 1
    @EditorField(displayLabel = "Count Fainted Pokémon", description = "Whether or not fainted Pokémon should count as a valid Pokémon. This is true when you plan on healing them first.")
    val countFaintedPokemon = false

    override fun passes(ctx: QuestContext): Boolean {
        val party = StorageProxy.getParty(ctx.player)
        return party.countAblePokemon() >= necessaryPokemon || (countFaintedPokemon && party.team.size >= necessaryPokemon)
    }
}