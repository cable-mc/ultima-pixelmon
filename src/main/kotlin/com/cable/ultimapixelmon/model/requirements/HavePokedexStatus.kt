package com.cable.ultimapixelmon.model.requirements

import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.quest.Quest
import com.cable.ultimaquests.api.model.requirement.Requirement
import com.cable.ultimaquests.api.model.validationResults
import com.cable.ultimaquests.api.util.sanitize
import com.pixelmonmod.pixelmon.api.pokedex.PokedexRegistrationStatus
import com.pixelmonmod.pixelmon.api.pokemon.species.Pokedex
import com.pixelmonmod.pixelmon.api.registries.PixelmonSpecies
import com.pixelmonmod.pixelmon.api.storage.StorageProxy

class HavePokedexStatus : Requirement() {
    @EditorField(displayLabel = "Species", description = "The Pokémon to check the Pokédex status of.")
    var species = ""
    @EditorField(displayLabel = "Status", description = "The minimum Pokédex status they must have.")
    val status = PokedexRegistrationStatus.CAUGHT

    override fun sanitize(quest: Quest<*>) {
        super.sanitize(quest)
        species = species.sanitize()
    }

    override fun validate() {
        super.validate()
        if (!PixelmonSpecies.has(species)) {
            validationResults.addFailure("Unrecognized species: ${species}")
        }
    }

    override fun passes(ctx: QuestContext): Boolean {
        val party = StorageProxy.getParty(ctx.player)

        val status = party.playerPokedex.get(Pokedex.nameToID(species))
        return status.ordinal >= this.status.ordinal
    }
}