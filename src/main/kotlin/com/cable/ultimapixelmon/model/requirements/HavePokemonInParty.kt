package com.cable.ultimapixelmon.model.requirements

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.requirement.Requirement
import com.cable.ultimaquests.api.model.validationResults
import com.pixelmonmod.pixelmon.api.storage.StorageProxy

open class HavePokemonInParty : Requirement() {
    @EditorField(displayLabel = "Pokémon", description = "The Pokémon to have as a space-separated Pokémon Spec such as pikachu lvl:15 shiny", type = "string")
    var spec = BLANK_SPEC
    @EditorField(displayLabel = "Slot", description = "The slot that the Pokémon must be in, 1 up to 6. -1 means it doesn't matter, and is the default.")
    var slot = -1

    override fun validate() {
        super.validate()
        if (slot != -1 && (slot < 1 || slot > 6)) {
            validationResults.addFailure("Slot should be between 1 and 6, or be -1.")
        }
    }

    override fun passes(ctx: QuestContext): Boolean {
        val party = StorageProxy.getParty(ctx.player)
        if (slot == -1) {
            return party.all.any { it != null && spec.matches(it) }
        } else {
            return party.get(slot - 1) != null && spec.matches(party.get(slot - 1))
        }
    }
}