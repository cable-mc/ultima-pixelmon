package com.cable.ultimapixelmon.model.requirements

import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.requirement.Requirement
import com.pixelmonmod.pixelmon.api.economy.BankAccountProxy

open class HaveMoney : Requirement() {
    @EditorField(displayLabel = "Amount", description = "The amount of money they need to have.")
    var amount = 100

    override fun passes(context: QuestContext): Boolean {
        val account = BankAccountProxy.getBankAccount(context.player).orElse(null) ?: return false
        return account.balance.toDouble() >= amount
    }
}