package com.cable.ultimapixelmon.model.actions

import com.cable.ultimapixelmon.UltimaPixelmon
import com.cable.ultimapixelmon.util.getPC
import com.cable.ultimapixelmon.util.getParty
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.action.Action
import com.cable.ultimaquests.api.model.validationResults
import com.pixelmonmod.pixelmon.api.storage.PCStorage
import com.pixelmonmod.pixelmon.api.storage.PokemonStorage
import com.pixelmonmod.pixelmon.api.storage.StoragePosition

open class BoxPokemon : Action() {
    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon to take, as a Pokémon Spec such as pikachu lvl:15 shiny", type = "string")
    var spec = UltimaPixelmon.BLANK_SPEC
    @EditorField(displayLabel = "Max to Take", description = "The maximum number of matching Pokémon to box")
    val maxToTake = 1
    @EditorField(displayLabel = "Specific Slot", description = "The specific position in the party to move. This should be between 1 and 6. -1 means it doesn't matter.")
    var slot = -1

    override fun validate() {
        super.validate()
        if (slot != -1 && slot !in 1..6) {
            validationResults.addFailure("box-pokemon slot number is neither -1 nor between 1-6")
        }
    }

    fun tryBox(pc: PCStorage, storage: PokemonStorage, position: StoragePosition): Boolean {
        val pokemon = storage.get(position)
        if (pokemon != null && spec.matches(pokemon)) {
            pc.transfer(storage, position, pc.firstEmptyPosition)
            return true
        }
        return false
    }

    override fun execute(ctx: QuestContext) {
        var taken = 0
        val party = ctx.player.getParty()
        val pc = ctx.player.getPC()

        if (slot != -1) {
            tryBox(pc, party, StoragePosition(-1, slot - 1))
        } else {
            for (slot in 0..5) {
                if (taken < maxToTake && tryBox(pc, party, StoragePosition(-1, slot))) {
                    taken++
                }
            }
        }
    }
}