package com.cable.ultimapixelmon.model.actions

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.action.Action
import com.cable.ultimaquests.api.model.task.Task
import com.cable.ultimaquests.api.model.validationResults
import com.cable.ultimaquests.api.util.sanitize
import com.cable.ultimaquests.internal.util.setMark
import com.pixelmonmod.api.pokemon.requirement.impl.SpeciesRequirement
import com.pixelmonmod.pixelmon.api.storage.StorageProxy
import com.pixelmonmod.pixelmon.items.HeldItem
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.nbt.JsonToNBT
import net.minecraft.util.ResourceLocation
import net.minecraftforge.registries.ForgeRegistries

open class GivePokemon : Action() {
    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon to give to the player, written as a Pokémon Spec", type = "string")
    var spec = BLANK_SPEC
    @EditorField(displayLabel = "Held Item ID", description = "The item ID of the item to have the Pokémon hold, such as pixelmon:focus_band.")
    var heldItem = ""
    @EditorField(displayLabel = "Held Item Mark", description = "The mark to apply to the item the Pokémon is made to hold.")
    var heldItemMark = ""
    @EditorField(displayLabel = "Held Item NBT", description = "The NBT to have on the item the Pokémon is made to hold.")
    var heldItemNBT = ""
    @EditorField(displayLabel = "Put in PC", description = "Whether it should be put straight into the PC, even if the party has space.")
    var putInPC = false

    override fun sanitize(task: Task<*, *>) {
        super.sanitize(task)
        heldItemMark = heldItemMark.sanitize()
        heldItem = heldItem.lowercase().trim()
    }

    override fun validate() {
        super.validate()
        if (spec.getValue(SpeciesRequirement::class.java)?.isPresent == false) {
            validationResults.addFailure("There is no name in the Pokémon Spec.")
        }

        if (heldItem.isNotBlank()) {
            val item = ForgeRegistries.ITEMS.getValue(ResourceLocation(heldItem)) ?: Items.AIR
            if (item == Items.AIR && heldItem != "minecraft:air") {
                validationResults.addFailure("give-pokemon has unrecognized item: $heldItem")
            } else if (item !is HeldItem) {
                validationResults.addFailure("give-pokemon has a held item that isn't an actual held item: $heldItem")
            }
        }

        if (heldItem.isBlank() && heldItemMark.isNotBlank()) {
            validationResults.addWarning("give-pokemon has heldItemMark but no held item. Weird.")
        }

        if (heldItem.isBlank() && heldItemNBT.isNotBlank()) {
            validationResults.addWarning("give-pokemon has heldItemNBT but no held item. Weird.")
        }

        if (heldItemNBT.isNotBlank()) {
            try {
                JsonToNBT.parseTag(heldItemNBT)
            } catch (e: Exception) {
                validationResults.addFailure("give-pokemon has invalid NBT: \"$heldItemNBT\"")
            }
        }
    }

    override fun execute(ctx: QuestContext) {
        val storage = if (putInPC) {
            StorageProxy.getPCForPlayer(ctx.player)
        } else {
            StorageProxy.getParty(ctx.player)
        }

        val pokemon = spec.create()

        if (heldItem.isNotBlank()) {
            val item = ctx.getItemRegistry().getValue(ResourceLocation(heldItem)) as HeldItem
            val heldItemStack = ItemStack(item)
            if (heldItemNBT.isNotBlank()) {
                heldItemStack.tag = JsonToNBT.parseTag(ctx.substituteInto(heldItemNBT))
            }
            if (heldItemMark.isNotBlank()) {
                heldItemStack.setMark(heldItemMark)
            }
            pokemon.heldItem = heldItemStack
        }

        storage.add(pokemon)
    }
}