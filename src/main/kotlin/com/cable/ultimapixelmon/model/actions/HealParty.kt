package com.cable.ultimapixelmon.model.actions

import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.action.Action
import com.pixelmonmod.pixelmon.api.storage.StorageProxy

class HealParty : Action() {
    override fun execute(ctx: QuestContext) {
        StorageProxy.getParty(ctx.player).heal()
    }
}