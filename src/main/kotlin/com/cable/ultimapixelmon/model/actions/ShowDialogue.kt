package com.cable.ultimapixelmon.model.actions

import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.action.Action
import com.cable.ultimaquests.api.model.task.Task
import com.cable.ultimaquests.api.model.validationResults
import com.pixelmonmod.pixelmon.api.dialogue.Dialogue

class ShowDialogue : Action() {
    @EditorField(displayLabel = "Chat Name", description = "The name to appear in the dialogue box. Defaults to blank.")
    var chatName = ""
    @EditorField(displayLabel = "ESC Closes GUI", description = "If true, pressing ESC will close the GUI and bypass the rest of the text. Defaults to false, please never set this to true unless you know what you're doing.")
    var escapeClosesGUI = false
    @EditorField(displayLabel = "Chat Pages", description = "The pages of text to show in the dialogue. On the final page, the choices will be shown.", type = "string")
    var chatPages = mutableListOf<String>()

    override fun sanitize(task: Task<*, *>) {
        super.sanitize(task)
        if (chatPages.isEmpty()) {
            chatPages.add("")
        }
    }

    override fun validate() {
        super.validate()
        if (chatPages.isEmpty()) {
            validationResults.addFailure("The chat pages list is empty. You probably want some pages here.")
        }
    }

    override fun execute(ctx: QuestContext) {
        val dialogue = chatPages.map {
            Dialogue.builder()
                .setName(ctx.substituteInto(chatName))
                .setText(ctx.substituteInto(it))
                .also { builder ->
                    if (escapeClosesGUI) {
                        builder.escapeCloses()
                    } else {
                        builder.requireManualClose()
                    }
                }
                .build()
        }

        Dialogue.setPlayerDialogueData(ctx.player, dialogue, true)
    }
}