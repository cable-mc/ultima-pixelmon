package com.cable.ultimapixelmon.model.actions

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimapixelmon.util.getPC
import com.cable.ultimapixelmon.util.getParty
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.action.Action
import com.cable.ultimaquests.api.model.validationResults
import com.pixelmonmod.pixelmon.api.config.PixelmonConfigProxy
import com.pixelmonmod.pixelmon.api.storage.PokemonStorage
import com.pixelmonmod.pixelmon.api.storage.StoragePosition

open class TakePokemon : Action() {
    @EditorField(displayLabel = "Pokémon Spec", description = "The Pokémon to take, as a Pokémon Spec such as pikachu lvl:15 shiny", type = "string")
    var spec = BLANK_SPEC
    @EditorField(displayLabel = "Where to Search", description = "Where to look for a matching Pokémon")
    var search = StorageSearchLocation.Party
    @EditorField(displayLabel = "Specific PC Box", description = "The specific PC box to search. If the search option is set to Party then this option will do nothing. -1 means it doesn't matter.")
    var box = -1
    @EditorField(displayLabel = "Specific Slot", description = "The specific position to search. If the search option is set to Party, this should be between 1 and 6. If PC, it is between 1 and 30. -1 means it doesn't matter.")
    var slot = -1
    @EditorField(displayLabel = "Max to Take", description = "The maximum number of matching Pokémon to take")
    val maxToTake = 1

    override fun validate() {
        super.validate()
        if (box != -1 && box !in 1..PixelmonConfigProxy.getStorage().computerBoxes) {
            validationResults.addFailure("take-pokemon box number is neither -1 nor within 1-${PixelmonConfigProxy.getStorage().computerBoxes}. $box is not a good value")
        }
        if (search != StorageSearchLocation.PC && slot != -1 && slot !in 1..6) {
            validationResults.addFailure("take-pokemon slot number is neither -1 nor between 1-6 and this is a search that can include the party")
        }
        if (slot != -1 && slot !in 1..30) {
            validationResults.addFailure("take-pokemon slot number is neither -1 nor between 1-30")
        }
        if (box != -1 && search == StorageSearchLocation.Party) {
            validationResults.addWarning("take-pokemon box number has been set, but the search is set to Party. A mistake?")
        }
    }

    fun tryRemove(storage: PokemonStorage, position: StoragePosition): Boolean {
        val pokemon = storage.get(position)
        if (pokemon != null && spec.matches(pokemon)) {
            storage.set(position, null)
            return true
        }
        return false
    }

    override fun execute(ctx: QuestContext) {
        var taken = 0
        val party = ctx.player.getParty()
        val pc = ctx.player.getPC()

        if (search == StorageSearchLocation.Party || search == StorageSearchLocation.Both) {
            if (slot != -1) {
                if (tryRemove(party, StoragePosition(-1, slot - 1))) {
                    taken++
                }
            } else {
                for (slot in 0..5) {
                    if (taken < maxToTake && tryRemove(party, StoragePosition(-1, slot))) {
                        taken++
                    }
                }
            }
        }

        if (taken < maxToTake && (search == StorageSearchLocation.PC || search == StorageSearchLocation.Both)) {
            val boxes = if (box == -1) pc.boxes.toList() else listOf(pc.getBox(box - 1))
            for (box in boxes) {
                if (slot != -1) {
                    if (taken < maxToTake && tryRemove(box, StoragePosition(-1, slot - 1))) {
                        taken++
                    }
                } else {
                    for (slot in 0..29) {
                        if (taken < maxToTake && tryRemove(box, StoragePosition(-1, slot))) {
                            taken++
                        }
                    }
                }
            }
        }
    }

    enum class StorageSearchLocation {
        Party,
        PC,
        Both
    }
}