package com.cable.ultimapixelmon.model.actions

import com.cable.ultimapixelmon.model.tasks.SelectChoice
import com.cable.ultimapixelmon.util.momentarily
import com.cable.ultimaquests.api.UltimaQuests
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.action.Action
import com.cable.ultimaquests.api.model.task.Task
import com.cable.ultimaquests.api.model.validationResults
import com.cable.ultimaquests.api.util.sanitize
import com.pixelmonmod.pixelmon.api.dialogue.Choice
import com.pixelmonmod.pixelmon.api.dialogue.Dialogue

open class ShowChoices : Action() {
    @EditorField(displayLabel = "Choice ID", description = "The name of the choice. This is so you can reference it in a choose-option task. Must be unique.")
    var choiceID = ""
    @EditorField(displayLabel = "Chat Name", description = "The name to appear in the dialogue box. Defaults to blank.")
    var chatName = ""
    @EditorField(displayLabel = "ESC Closes GUI", description = "If true, pressing ESC will close the GUI and bypass having to choose. Defaults to false, please never set this to true unless you know what you're doing.")
    var escapeClosesGUI = false
    @EditorField(displayLabel = "Chat Pages", description = "The pages of text to show in the dialogue. On the final page, the choices will be shown.", type = "string")
    var chatPages = mutableListOf<String>()
    @EditorField(displayLabel = "Choices", description = "The choices to select from.", type = "string")
    var choices = mutableListOf<String>()
    override fun sanitize(task: Task<*, *>) {
        super.sanitize(task)
        choiceID.sanitize()
        if (chatPages.isEmpty()) {
            chatPages.add("")
        }
    }

    override fun validate() {
        super.validate()
        if (choices.isEmpty()) {
            validationResults.addFailure("The choice list is empty. You probably want some choices here.")
        }
    }

    override fun execute(ctx: QuestContext) {
        val dialogueBuilders = chatPages.map {
            Dialogue.builder()
                .setName(ctx.substituteInto(chatName))
                .setText(ctx.substituteInto(it))
                .also { builder ->
                    if (escapeClosesGUI) {
                        builder.escapeCloses()
                    } else {
                        builder.requireManualClose()
                    }
                }
        }

        val lastDialogue = dialogueBuilders.last()
        choices.forEachIndexed { index, choice ->
            lastDialogue.addChoice(
                Choice.builder()
                    .setText(ctx.substituteInto(choice))
                    .setHandle {
                        momentarily {
                            UltimaQuests.api.getTaskProcessor().processAllFor(
                                clazz = SelectChoice::class.java,
                                player = ctx.player,
                                data = ChoiceData(
                                    choiceID = choiceID,
                                    choiceNumber = index + 1
                                )
                            )
                        }
                    }
                    .build()
            )
        }

        Dialogue.setPlayerDialogueData(ctx.player, dialogueBuilders.map { it.build() }, true)
    }
}

data class ChoiceData(
    val choiceID: String,
    val choiceNumber: Int
)