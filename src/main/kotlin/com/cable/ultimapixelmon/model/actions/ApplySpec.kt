package com.cable.ultimapixelmon.model.actions

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.cable.ultimapixelmon.util.getPC
import com.cable.ultimapixelmon.util.getParty
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.action.Action
import com.pixelmonmod.pixelmon.api.storage.PokemonStorage

class ApplySpec : Action() {
    @EditorField(displayLabel = "Target Pokémon Spec", description = "The spec to use to find what Pokémon will be changed.", type = "string")
    var target = BLANK_SPEC
    @EditorField(displayLabel = "Applied Pokémon Spec", description = "The spec to apply to the matched Pokémon.", type = "string")
    var applied = BLANK_SPEC
    @EditorField(displayLabel = "Apply Count", description = "How many matching Pokémon this spec should apply to. If -1, it will apply it to all that match.")
    var applyCount = 1
    @EditorField(displayLabel = "Checked Storage", description = "Which storage to search for matching Pokémon.")
    var storageType = StorageType.PARTY

    enum class StorageType {
        PARTY,
        PC,
        BOTH
    }

    override fun execute(ctx: QuestContext) {
        if (storageType == StorageType.PARTY) {
            processStore(ctx.player.getParty(), applyCount)
        } else if (storageType == StorageType.PC) {
            processStore(ctx.player.getPC(), applyCount)
        } else {
            val remainingToApply = processStore(ctx.player.getParty(), applyCount)
            if (remainingToApply != 0) {
                processStore(ctx.player.getPC(), remainingToApply)
            }
        }
    }

    fun processStore(store: PokemonStorage, remainingApply: Int): Int {
        var applied = remainingApply
        for (pokemon in store.all) {
            if (pokemon != null && target.matches(pokemon)) {
                applied--
                this.applied.apply(pokemon)
                if (applied == 0) {
                    return 0
                }
            }
        }
        return applied
    }
}