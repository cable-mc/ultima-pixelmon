package com.cable.ultimapixelmon.model.actions

import com.cable.ultimapixelmon.battles.WatchedBattleController
import com.cable.ultimaquests.api.UltimaQuests
import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.action.Action
import com.cable.ultimaquests.api.model.task.Task
import com.cable.ultimaquests.api.model.validationResults
import com.pixelmonmod.api.pokemon.PokemonSpecification
import com.pixelmonmod.api.pokemon.requirement.impl.SpeciesRequirement
import com.pixelmonmod.pixelmon.api.battles.BattleEndCause
import com.pixelmonmod.pixelmon.api.battles.BattleType
import com.pixelmonmod.pixelmon.api.storage.StorageProxy
import com.pixelmonmod.pixelmon.battles.BattleRegistry
import com.pixelmonmod.pixelmon.battles.api.rules.BattleRuleRegistry
import com.pixelmonmod.pixelmon.battles.api.rules.BattleRules
import com.pixelmonmod.pixelmon.battles.controller.participants.BattleParticipant
import com.pixelmonmod.pixelmon.battles.controller.participants.PlayerParticipant
import com.pixelmonmod.pixelmon.battles.controller.participants.WildPixelmonParticipant

class SpawnAndBattle : Action() {
    @EditorField(displayLabel = "X", description = "The x coordinate to spawn the opponent Pokémon at. If left at zero, they spawn on the player.")
    var x = 0.0
    @EditorField(displayLabel = "Y", description = "The y coordinate to spawn the opponent Pokémon at. If left at zero, they spawn on the player.")
    var y = 0.0
    @EditorField(displayLabel = "Z", description = "The z coordinate to spawn the opponent Pokémon at. If left at zero, they spawn on the player.")
    var z = 0.0
    @EditorField(displayLabel = "Opponents", description = "The Pokémon to spawn as opponents", type = "string")
    var opponents = mutableListOf<PokemonSpecification>()
    @EditorField(displayLabel = "Allies", description = "The Pokémon to spawn as allies", type = "string")
    var allies = mutableListOf<PokemonSpecification>()
    @EditorField(displayLabel = "Heal First", description = "Whether or not the player should be healed first.")
    var healFirst = false
    @EditorField(displayLabel = "Battle Rules", description = "The battle rules text. Multi-line text is hard in this editor, so use ; whenever you want a new line.")
    var battleRules = ""
    @EditorField(displayLabel = "Victory Actions", description = "The actions to run if the player wins the battle.", type = "action")
    var onVictory = mutableListOf<Action>()
    @EditorField(displayLabel = "Defeat Actions", description = "The actions to run if the player loses the battle.", type = "action")
    var onDefeat = mutableListOf<Action>()
    @EditorField(displayLabel = "Draw Actions", description = "The actions to run if the player ties the battle. Only possible with some pixelmon.hocon settings.", type = "action")
    var onDraw = mutableListOf<Action>()
    @EditorField(displayLabel = "Flee Actions", description = "The actions to run if the player flees the battle. Only possible if there was no battle rule to prevent fleeing.", type = "action")
    var onFlee = mutableListOf<Action>()
    @EditorField(displayLabel = "Error Actions", description = "The actions to run if the player disconnects or there's some other error with the battle.", type = "action")
    var onError = mutableListOf<Action>()

    override fun sanitize(task: Task<*, *>) {
        super.sanitize(task)
        onVictory.forEach { it.sanitize(task) }
        onDefeat.forEach { it.sanitize(task) }
        onDraw.forEach { it.sanitize(task) }
        onFlee.forEach { it.sanitize(task) }
        onError.forEach { it.sanitize(task) }
    }

    override fun validate() {
        super.validate()
        if (opponents.isEmpty()) {
            validationResults.addFailure("The opponents list is empty. It isn't meant to be.")
        }
        if (opponents.any { it.getValue(SpeciesRequirement::class.java)?.isPresent == false }) {
            validationResults.addFailure("The opponents list contains an invalid or nonspecified Pokémon")
        }
        if (opponents.size > 2) {
            validationResults.addFailure("Opponents list has more than 2 elements. Only single and double battles are currently supported.")
        }
        if (allies.size > 1) {
            validationResults.addFailure("Allies list has more than 1 element. Only single and double battles are supported. This option is a list only in preparation for triple and rotation battles.")
        }
        if (allies.any { it.getValue(SpeciesRequirement::class.java)?.isPresent == false }) {
            validationResults.addFailure("The allies list contains an invalid or nonspecified Pokémon")
        }
        val battleRules = BattleRules()
        battleRules.importText(this.battleRules.split(";").joinToString(separator = "\n"))
            ?.let { validationResults.addFailure("Failure in battle rules. The issue was at: $it") }
        onVictory.forEach { it.validate() }
        onDefeat.forEach { it.validate() }
        onDraw.forEach { it.validate() }
        onFlee.forEach { it.validate() }
        onError.forEach { it.validate() }
    }

    override fun execute(ctx: QuestContext) {
        BattleRegistry.getBattle(ctx.player)?.endBattle(BattleEndCause.FORCE)
        val party = StorageProxy.getParty(ctx.player)
        if (healFirst) {
            party.heal()
        }
        val allyParticipants: List<BattleParticipant>
        val opponentParticipants: List<WildPixelmonParticipant>
        val battleRules = BattleRules().also { it.importText(this.battleRules.split(";").joinToString(separator = "\n")) }
        if (opponents.size == 2) {
            battleRules.set(BattleRuleRegistry.BATTLE_TYPE, BattleType.DOUBLE)
            opponentParticipants = opponents.map { spec ->
                val pixelmon = spec.create().getOrSpawnPixelmon(ctx.player)
                if (x != 0.0 || y != 0.0 || z != 0.0) {
                    pixelmon.setPos(x, y, z)
                }
                return@map WildPixelmonParticipant(pixelmon)
            }
            if (allies.size > 0) {
                allyParticipants = listOf(
                    WildPixelmonParticipant(allies.first().create().getOrSpawnPixelmon(ctx.player)),
                    PlayerParticipant(ctx.player, party.getAndSendOutFirstAblePokemon(ctx.player))
                )
            } else {
                val ablePokemon = party.team.filter { it.canBattle() }
                if (ablePokemon.size < 2) {
                    UltimaQuests.logError("Unable to spawn-and-battle for task: ${getTask().name} in quest: ${getQuest().name} because ${ctx.player.name} didn't have 2 Pokémon. Should you have checked using the can-battle condition before doing this?")
                    onError.forEach { it.schedule(ctx) }
                    return
                }
                allyParticipants = listOf(
                    PlayerParticipant(
                        ctx.player,
                        *ablePokemon.subList(0, 2).map { it.getOrSpawnPixelmon(ctx.player) }.toTypedArray()
                    )
                )
            }
        } else if (opponents.size == 1) {
            val pixelmon = opponents.first().create().getOrSpawnPixelmon(ctx.player)
            if (x != 0.0 || y != 0.0 || z != 0.0) {
                pixelmon.setPos(x, y, z)
            }
            opponentParticipants = listOf(WildPixelmonParticipant(pixelmon))
            allyParticipants = listOf(PlayerParticipant(ctx.player, party.getAndSendOutFirstAblePokemon(ctx.player)))
        } else {
            return // Not likely until triple battles
        }

        val bc = WatchedBattleController(
            ctx = ctx,
            onVictory = this.onVictory,
            onDefeat = this.onDefeat,
            onDraw = this.onDraw,
            onFlee = this.onFlee,
            onError = this.onError,
            team1 = allyParticipants.toTypedArray(),
            team2 = opponentParticipants.toTypedArray(),
            rules = battleRules
        )

        BattleRegistry.registerBattle(bc)
    }
}