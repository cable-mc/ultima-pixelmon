package com.cable.ultimapixelmon.model.actions

import com.cable.ultimaquests.api.editor.EditorField
import com.cable.ultimaquests.api.model.QuestContext
import com.cable.ultimaquests.api.model.action.Action
import com.cable.ultimaquests.api.model.validationResults
import com.pixelmonmod.pixelmon.api.economy.BankAccountProxy
import kotlin.math.abs

class ChangeMoney : Action() {
    @EditorField(displayLabel = "Amount", description = "The difference to apply to the player's balance. A negative value means that money will be removed.")
    var amount = 0

    override fun validate() {
        super.validate()
        if (amount == 0) {
            validationResults.addWarning("Amount is zero. Why?")
        }
    }

    override fun execute(ctx: QuestContext) {
        val account = BankAccountProxy.getBankAccount(ctx.player).orElse(null) ?: return
        if (amount > 0) {
            account.add(amount)
        } else if (amount < 0) {
            if (account.balance.toDouble() <= abs(amount)) {
                account.take(account.balance)
            } else {
                account.take(abs(amount))
            }
        }
    }
}