package com.cable.ultimapixelmon.util

import com.cable.ultimapixelmon.UltimaPixelmon.Companion.BLANK_SPEC
import com.pixelmonmod.api.pokemon.PokemonSpecification
import com.pixelmonmod.pixelmon.api.storage.PCStorage
import com.pixelmonmod.pixelmon.api.storage.PlayerPartyStorage
import com.pixelmonmod.pixelmon.api.storage.StorageProxy
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.nbt.CompoundNBT

fun ServerPlayerEntity.getParty(): PlayerPartyStorage = StorageProxy.getParty(this)
fun ServerPlayerEntity.getPC(): PCStorage = StorageProxy.getPCForPlayer(this)

fun PokemonSpecification.isBlank() = this.write(CompoundNBT()).toString() == BLANK_SPEC.write(CompoundNBT()).toString()