package com.cable.ultimapixelmon.util

import com.cable.library.event.subscribeOnce
import net.minecraftforge.event.TickEvent

fun momentarily(block: () -> Unit) {
    subscribeOnce<TickEvent.ServerTickEvent>(condition = { it.phase == TickEvent.Phase.START }) { block() }
}