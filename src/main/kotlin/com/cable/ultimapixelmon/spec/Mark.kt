package com.cable.ultimapixelmon.spec

import com.cable.ultimaquests.api.util.sanitize
import com.pixelmonmod.api.pokemon.requirement.AbstractStringPokemonRequirement
import com.pixelmonmod.api.requirement.Requirement
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon
import com.pixelmonmod.pixelmon.entities.pixelmon.PixelmonEntity

class MarkRequirement() : AbstractStringPokemonRequirement(setOf("mark"), "") {
    constructor(mark: String): this() {
        this.value = mark
    }

    override fun createInstance(mark: String): Requirement<Pokemon, PixelmonEntity, String> {
        return MarkRequirement(mark)
    }

    override fun isDataMatch(pokemon: Pokemon): Boolean {
        return pokemon.persistentData.getString("mark") == value.sanitize()
    }

    override fun applyData(pokemon: Pokemon) {
        pokemon.persistentData.putString("mark", value.sanitize())
    }
}