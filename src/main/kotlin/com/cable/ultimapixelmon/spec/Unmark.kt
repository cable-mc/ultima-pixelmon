package com.cable.ultimapixelmon.spec

import com.pixelmonmod.api.pokemon.requirement.AbstractStringPokemonRequirement
import com.pixelmonmod.api.requirement.Requirement
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon
import com.pixelmonmod.pixelmon.entities.pixelmon.PixelmonEntity

class UnmarkRequirement() : AbstractStringPokemonRequirement(setOf("unmark"), "") {
    override fun createInstance(mark: String): Requirement<Pokemon, PixelmonEntity, String> {
        return UnmarkRequirement()
    }

    override fun isDataMatch(pokemon: Pokemon): Boolean {
        return pokemon.persistentData.getString("mark").isBlank()
    }

    override fun applyData(pokemon: Pokemon) {
        pokemon.persistentData.remove("mark")
    }
}